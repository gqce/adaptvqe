# author: Yongxin Yao (yxphysice@gmail.com)
import numpy, h5py
import scipy.optimize
from timing import timeit


class ansatz:
    '''
    define the main procedures of adapt-vqe calculation.
    set up: 1) default referece state of the ansatz.
            2) operator pool
    '''
    def __init__(self,
            model,           # the qubit model
            opspm=True,      # operator in scipy sparse matrix
            refmode=0,       # reference state mode
            pmode=0,         # pool choice. 0: sUCCpD pool, 1: commutator pool
            maxadd=1,        # maximal ops to be added each iteration.
            tol=1e-3,        # minimal gradient
            optmode=0,       # 0:BFGS, 1: seq_batch_opt
            maxiter=10000,   # maximally allowed adaptive steps.
            filterz=True,    # filter pool by removing Zs
            mingenerators=False,  # minimize generator types.
            ):
        # variational parameters.
        self._params = []
        # list of unitaries in terms of operators and labels
        self._ansatz = [[], []]
        self._opspm = opspm
        self._model = model
        self._nq = model._nsite
        self._refmode = refmode
        self._optmode = optmode
        self._maxiter = maxiter
        self._filterz = filterz
        self._mingenerators = mingenerators

        # set reference state
        self.set_ref_state()
        self._state = None
        # generate operator
        self.setup_pool(pmode)
        if maxadd == 0:
            maxadd = len(self._pool)
        self._maxadd = min(maxadd, len(self._pool))
        self._tol = tol

    @timeit
    def additive_optimize(self):
        # change the representation of H if necessary.
        if self._opspm:
            self._h = self._model._h.data.tocsr()
        else:
            self._h = self._model._h
        # (adaptively) optimize the ansatz to represent the groud state
        # of the model Hamiltonian.
        self.optimize_params()
        print(f"initial cost = {self._cost:.6f}")
        iloop = 0
        with open('record.dat', 'w') as f:
            f.write(f"{len(self._params)} {self._cost:.8f} {self.ncnots}\n")
            while True:
                # one iteration of qubit-adapt-vqe.
                added = self.add_op()
                if not added:
                    # reaching convergence.
                    break
                self.optimize_params()
                self.update_state()
                print(f"ngates: {self._ngates}")
                print(f"iter {iloop}: cost = {self._cost:.6f}")
                f.write(f"{len(self._params)} {self._cost:.8f} " +
                        f"{self.ncnots}\n")
                f.flush()
                iloop += 1
                if iloop > self._maxiter:
                    break

    @property
    def state(self):
        return self._state

    @property
    def ngates(self):
        return self._ngates[:]

    @property
    def ncnots(self):
        ncnots = [i*2*e for i, e in enumerate(self._ngates)]
        return sum(ncnots)

    def get_cost(self):
        '''
        get the value of the cost function (Hamiltonian expectation value).
        '''
        # have the state vector updated.
        self.update_state()
        # expectation value of the Hamiltonian
        if self._opspm:
            res = self._state.conj().dot(self._h.dot(self._state))
        else:
            res = self._h.matrix_element(self._state, self._state)
        return res.real

    @timeit
    def update_state(self):
        self._state = self.get_state()

    def setup_pool(self,
            pmode,
            ):
        '''
        setup pool from pool.inp
        '''
        # labels = self._model._incar["pool"]
        # remove Z's and Identity.
        one = 'I'*self._nq
        labels = []
        if pmode==0:
            plabel = "pool"
        else:
            plabel = "pool_commutator"

        print(f'raw pool dimension: {len(self._model._incar[plabel])}')
        for label in self._model._incar[plabel]:
            if self._filterz:
                label = label.replace('Z', 'I')
            if label != one and label not in labels:
                labels.append(label)

        self._pool = [[self._model.label2op(s), s] for s in labels]
        self._ngates = [0]*len(self._pool[0][1])
        print(f'refined pool dimension: {len(self._pool)}')

        if self._opspm:
            # convert to more efficient data structure if needed.
            for i, op in enumerate(self._pool):
                self._pool[i][0] = op[0].data.tocsr()

    def set_ref_state(self):
        '''set reference state from ref_state.inp file.
        '''
        if self._refmode in [0, 1]:
            ref = self._model._incar["ref_state"]
            # binary literal to int
            nnz = int(ref, 2)
            vec = numpy.zeros((2**self._nq), dtype=numpy.complex_)
            vec[nnz] = 1.
            if self._refmode == 1:
                # apply givens rotation
                u = numpy.asarray(self._model._incar ['mo_coeff'])
                from givens import get_givens_labels, get_options
                from qiskit_nature.mappers.second_quantization import (
                        ParityMapper, JordanWignerMapper)

                conf = get_options()
                mapping = conf.get("qubit_encoding", 'parity')
                if mapping == 'parity':
                    mapper = ParityMapper()
                    two_qubit_reduction = True
                    z2symmetry_reduction = "auto"
                else:
                    mapper = JordanWignerMapper()
                    two_qubit_reduction = False
                    z2symmetry_reduction = None

                gens_list = get_givens_labels(u.T,
                        mapper=mapper,
                        two_qubit_reduction=two_qubit_reduction,
                        z2symmetry_reduction=z2symmetry_reduction,
                        )

                for cop in gens_list[::-1]:
                    theta, op_label = cop.split('*')
                    theta = float(theta)
                    op = self._model.label2op(op_label).data.tocsr()
                    vec = numpy.cos(theta)*vec + \
                            1j*numpy.sin(theta)*op.dot(vec)
            self._ref_state = vec
        else:
            h = self._model.coeflabels2op(self._model._incar['h1ao'])
            npenalty = self._model.get_npenalty()
            h += npenalty
            w, v = h.groundstate()
            print(f"<n_penalty>: {abs(npenalty.matrix_element(v, v)):.2e}")
            self._ref_state = v.full().reshape(-1)


    @timeit
    def optimize_params(self):
        if len(self._params) > 0:
            if self._optmode == 0:
                # full reoptimization of the ansatz given the initial point.
                res = scipy.optimize.minimize(fun_cost,
                        self._params,            # starting parameter point
                        args=(self._ansatz[0],   # only need the ansatz
                                                 # operators, not the indices.
                                self._ref_state, # reference state
                                self._h,         # Hamiltonian
                                self._model._indices0,   # for analysis only
                                ),
                        method='BFGS',           #'BFGS', Failed with SLSQP
                                                 # L-BFGS-B perfomrs worse.
                        jac=fun_jac,             # analytical jacobian function
                        )
                if res.success:
                    # save parameters and cost.
                    self._params = res.x.tolist()
                    self._cost = res.fun
                else:
                    print(res.message)
            elif self._optmode == 1:
                from opt import seq_batch_opt
                self._params, self._cost = seq_batch_opt(fun_cost,
                        self._params,
                        order=1,
                        args=(self._ansatz[0],   # only need the ansatz
                                                 # operators, not the indices.
                                self._ref_state, # reference state
                                self._h,         # Hamiltonian
                                self._model._indices0,  # for analysis only
                                ),
                        )
            elif self._optmode == 2:
                from opt import adadelta as optimizer
                self._params, self._cost = optimizer(fun_cost,
                        self._params,
                        jac=fun_jac,
                        args=(self._ansatz[0],   # only need the ansatz
                                                 # operators, not the indices.
                                self._ref_state, # reference state
                                self._h,         # Hamiltonian
                                self._model._indices0,  # for analysis only
                                ),
                        maxiter=200,
                        )
            else:
                # spsa
                from pygrisb.gsolver.qiskit.spsa import SPSA
                optimizer = SPSA(maxiter=100)
                def objective_function(x):
                    return fun_cost(x,
                            self._ansatz[0],
                            self._ref_state,
                            self._h,
                            self._model._indices0,
                            )
                self._params, self._cost, nfev = optimizer.optimize(
                        len(self._params),
                        objective_function,
                        initial_point=self._params,
                        )
                self._params = list(self._params)
        else:
            # no parameters to optimize, directly evaluate.
            self._cost = self.get_cost()

    @timeit
    def add_op(self):
        '''
        adding a initary  in the adapt-vqe.
        '''
        scores = self.get_pool_scores()
        ids = numpy.argsort(abs(scores))[::-1]
        iadds = [i for i in ids if abs(scores[i]) > self._tol]
        if len(iadds) == 0:
            print("converge: gradient too small.")
            return False
        else:
            print(f"choose {min(len(iadds), self._maxadd)} scores: "+ \
                    f"{' '.join(f'{scores[i]:.4f}' for i in iadds)}")
            if self._maxadd == 1 and self._mingenerators:
                ich = iadds[0]
                if self._pool[ich][1] not in self._ansatz[1] and \
                        len(set(self._ansatz[1])) == 2*self._nq-2:
                    # prefer operator already used
                    for i in iadds[1:]:
                        if abs(scores[i]-scores[ich]) < 10 and \
                                self._pool[i][1] in self._ansatz[1]:
                            ich = i
                            break
                self._ansatz[0].append(self._pool[ich][0])
                self._ansatz[1].append(self._pool[ich][1])
                self.update_ngates()
                print(f"op {self._ansatz[1][-1]} appended.")
                self._params.append(0.)
            else:
                for iadd in iadds[:self._maxadd]:
                    if len(self._ansatz[0]) > 0 and \
                            self._pool[iadd][1] == self._ansatz[1][-1]:
                        continue
                    self._ansatz[0].append(self._pool[iadd][0])
                    # label
                    self._ansatz[1].append(self._pool[iadd][1])
                    self.update_ngates()
                    print(f"op {self._ansatz[1][-1]} appended.")
                    self._params.append(0.)
            return True

    def update_ngates(self):
        '''
        update gate counts.
        '''
        label = self._ansatz[1][-1]
        iorder = len(label) - label.count('I')
        self._ngates[iorder-1] += 1

    def get_state(self):
        return get_ansatz_state(self._params,
                        self._ansatz[0],
                        self._ref_state,
                        )

    def get_pool_scores(self):
        '''-0.5j <[h,op]> = im(<h op>)
        '''
        scores = []

        if self._model._nfilter:
            state = self._state.copy()
            state[self._model._indices0] = 0
            state /= numpy.linalg.norm(state)
        else:
            state = self._state

        if self._opspm:
            h_vec = self._h.dot(state)
        else:
            h_vec = self._h*state
        for op in self._pool:
            op = op[0]
            if self._opspm:
                ov = op.dot(state)
                zes = numpy.vdot(h_vec, ov)
            else:
                ov = op*state
                zes = h_vec.overlap(ov)
            scores.append(zes.imag)
        return numpy.array(scores)

    def save_ansatz(self):
        with h5py.File("ansatz.h5", "w") as f:
            # initial state params
            f["/params"] = self._params_init
            # ansatz operator labels
            f["/ansatz_code"] = self._ansatz[1]
            # ngates
            f["/ngates"] = self._ngates
            # reference state
            f["/ref_state"] = self._ref_state

    def save_state(self, t):
        with h5py.File("state.h5", "w") as f:
            f["t"] = t
            if self._opspm:
                f["state"] = self._state
            else:
                f["state"] = self._statevec.full().reshape(-1)


def fun_cost(params, ansatz, ref_state, h, indices0):
    state = get_ansatz_state(params, ansatz, ref_state)
    if isinstance(state, numpy.ndarray):
        if indices0 is not None:
            state[indices0] = 0
            state /= numpy.linalg.norm(state)
        res = numpy.vdot(state, h.dot(state))
    else:
        res = h.matrix_element(state, state)
    return res.real


def fun_jac(params, ansatz, ref_state, h, indices0):
    # - d <var|h|var> / d theta
    np = len(ansatz)
    vec = get_ansatz_state(params, ansatz, ref_state)
    opspm = isinstance(vec, numpy.ndarray)
    # <vec|h
    if opspm:
        if indices0 is not None:
            vec[indices0] = 0
            vec /= numpy.linalg.norm(vec)

        h_vec = h.dot(vec)
    else:
        h_vec = h*vec

    jac = []
    state_i = ref_state
    if opspm:
        for i in range(np):
            op = ansatz[i]
            state_i = numpy.cos(0.5*params[i])*state_i - \
                    1j*numpy.sin(0.5*params[i])*op.dot(state_i)
            state = op.dot(state_i)
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                state = numpy.cos(0.5*theta)*state - \
                        1j*numpy.sin(0.5*theta)*op.dot(state)
            if indices0 is not None:
                state[indices0] = 0
                state /= numpy.linalg.norm(state)

            zes = numpy.vdot(h_vec, state)
            jac.append(zes.imag)
    else:
        for i in range(np):
            opth = -0.5j*params[i]*ansatz[i]
            state_i = opth.expm()*state_i
            state = op*state_i
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                opth = -0.5j*theta*op
                state = opth.expm()*state
            zes = h_vec.overlap(state)
            jac.append(zes.imag)
    res = numpy.array(jac)
    return res


def get_ansatz_state(params, ansatz, ref_state):
    state = ref_state
    opspm = isinstance(state, numpy.ndarray)
    for theta, op in zip(params, ansatz):
        if opspm:
            state = numpy.cos(0.5*theta)*state - \
                    1j*numpy.sin(0.5*theta)*op.dot(state)
        else:
            opth = -0.5j*theta*op
            state = opth.expm()*state
    return state
