# author: Yongxin Yao (yxphysice@gmail.com)
import json, numpy
from qutip import qeye, sigmax, sigmay, sigmaz, tensor, Qobj


class model:
    '''
    Base class. defines some common functions for various spin models.
    '''
    def __init__(self,
            wpenalty=15,  # prefactor of pentalty terms
            hmode=0,       # h in mo representation.
            nfilter=False, # for analysis only if True, not working.
            ):
        '''
        read h.inp and set up the Hamiltonian.
        '''
        self._wpenalty = wpenalty
        self._hmode = hmode
        self._nfilter = nfilter
        self.load_incar()
        self.setup_h()
        self.add_npenalty()

    def get_h_expval(self, vec):
        '''
        get Hamiltonian expectation value.
        '''
        # convert the state vector vec to Q object if it is an array.
        if isinstance(vec, numpy.ndarray):
            vec = Qobj(vec)
        # return the real expectation value.
        return self._h.matrix_element(vec, vec).real

    def get_loweste_states(self, h=None):
        '''
        get the lowest three eigenvalues and eigenstates.
        '''
        # get the lowest three eigenvalues and eigenstates.
        if h is None:
            h = self._h
        w, v = h.eigenstates(eigvals=3, sparse=self._nsite>10)
        return w, v

    def load_incar(self):
        self._incar = json.load(open("incar", "r"))

    def setup_h(self):
        if self._hmode == 0:
            hs_list = self._incar["h"]
            print("choose mo representation.")
        else:
            hs_list = self._incar["hao"]
            print("choose ao representation.")
        print(f'Hamiltonian terms: {len(hs_list)}')
        self._nsite = len(hs_list[0].split("*")[1])
        self.set_ops()
        self._h = self.coeflabels2op(hs_list)

    def add_npenalty(self):
        '''add (ntot_op -n_e)**2 if ntot.inp is present.
        '''
        op_pentaly = self.get_npenalty()
        if op_pentaly is not None:
            self._h += op_pentaly

    def get_npenalty(self):
        if "ntot" in self._incar:
            ns_list = self._incar["ntot"]
            n_op = self.coeflabels2op(ns_list)
            nume = self._incar["nume"]
            if self._nfilter:
                self._indices0 = numpy.where(abs(n_op.diag()-nume) > 0.1)
            else:
                self._indices0 = None
            op_pentaly = self._wpenalty*(n_op - nume)**2
            if "chempot" in self._incar:
                op_pentaly -= self._incar["chempot"]*n_op
            return op_pentaly
        else:
            self._indices0 = None
            return None

    def chk_chempot(self):
        ns_list = self._incar["ntot"]
        n_op = self.coeflabels2op(ns_list)
        nume = self._incar["nume"]
        op = self._h + 20*(n_op - nume - 2)**2
        ep2, _ = self.get_loweste_states(h=op)
        op = self._h + 20*(n_op - nume - 1)**2
        ep1, _ = self.get_loweste_states(h=op)
        op = self._h + 20*(n_op - nume + 1)**2
        em1, _ = self.get_loweste_states(h=op)
        op = self._h + 20*(n_op - nume + 2)**2
        em2, _ = self.get_loweste_states(h=op)
        op = self._h + 20*(n_op - nume)**2
        e0, _ = self.get_loweste_states(h=op)
        mu1 = max(-em1[0]+e0[0], (-em2[0]+e0[0])/2)
        mu2 = min(ep1[0]-e0[0], (ep2[0]-e0[0])/2)
        mu = (mu1+mu2)/2
        op = self._h - mu*n_op
        w, v = self.get_loweste_states(h=op)
        print(f'eigen energies: {w}')
        nele = n_op.matrix_element(v[0], v[0]).real
        print(f'chem = {mu:.6f} nele = {nele:.2f}')

    def coeflabels2op(self, clabels):
        op = 0
        for clabel in clabels:
            coef, label = clabel.split("*")
            op1 = self.label2op(label)
            op += op1*float(coef)
        return op

    def label2op(self, label):
        op = 1
        for i, s in enumerate(label):
            if s in ["X", "Y", "Z"]:
                op *= self._ops[s][i]
        return op

    def set_ops(self):
        '''
        set up site-wise sx, sy, sz operators.
        '''
        self._ops = {}
        self._ops["X"], self._ops["Y"], self._ops["Z"] = \
                get_sxyz_ops(self._nsite)

    def get_h(self):
        '''
        get Hamiltonian.
        '''
        return self._h



def get_sxyz_ops(nsite):
    '''
    set up site-wise sx, sy, sz operators.
    '''
    si = qeye(2)
    sx = sigmax()
    sy = sigmay()
    sz = sigmaz()
    sx_list = []
    sy_list = []
    sz_list = []

    op_list = [si for i in range(nsite)]
    for i in range(nsite):
        op_list[i] = sx
        sx_list.append(tensor(op_list))
        op_list[i] = sy
        sy_list.append(tensor(op_list))
        op_list[i] = sz
        sz_list.append(tensor(op_list))
        # reset
        op_list[i] = si
    return [sx_list, sy_list, sz_list]
