#!/usr/bin/env python

from mpi4py import MPI
from qiskit import QuantumCircuit, execute, QuantumRegister, ClassicalRegister
import warnings
from qiskit_nature.mappers.second_quantization import ParityMapper
from qiskit import Aer
from qiskit.quantum_info import Pauli
from qiskit.opflow.primitive_ops import PauliSumOp, PauliOp
from openfermion.linalg import givens_decomposition_square as givensRotns
from qiskit.providers.aer import AerSimulator
from qiskit_nature.converters.second_quantization import QubitConverter
import copy, json
import numpy, time, h5py, itertools, os, pickle

from qiskit_nature.operators.second_quantization.fermionic_op import  FermionicOp
from qiskit.circuit import Parameter

from timing import timeit



def warn(*args, **kwargs):
    pass

warnings.warn = warn


def exp_all_z(
        circuit,
        pauli_ids,
        t,
        quantum_register=None,
        control_qubit=None,
        ):
    """
    origin: https://github.com/DavitKhach/quantum-algorithms-tutorials
    The implementation of exp(-iZZ..Z t), where Z is
    the Pauli Z operator, t is a parameter.
    :param circuit: QuantumCircuit.
    :param quantum_register: QuantumRegister.
    :param pauli_ids: the indexes from quantum_register that
                         correspond to entries not equal to I:
                         e.g. if we have XIYZI then the
                         pauli_ids = [0,2,3].
    :param control_qubit: the control Qubit from QuantumRegister
                          other than quantum_register.
    :param t: the parameter t in exp(iZZ..Z t).
    """
    # the controlled_exp(iIt) special case
    if len(pauli_ids) == 0 and control_qubit is not None:
        circuit.add_register(control_qubit.register)
        circuit.u1(-t, control_qubit)
        return

    if quantum_register is None:
        quantum_register = QuantumRegister(circuit.num_qubits, 'q')

    # the first CNOTs
    for i, j in zip(pauli_ids, pauli_ids[1:]):
        circuit.cx(quantum_register[i], quantum_register[j])

    # Rz gate
    if control_qubit is None:
        circuit.rz(2*t, quantum_register[pauli_ids[-1]])
    else:
        circuit.add_register(control_qubit.register)
        circuit.crz(2*t,
                control_qubit,
                quantum_register[pauli_ids[-1]]
                )

    # the second CNOTs
    for i, j in zip(pauli_ids[-2::-1], pauli_ids[::-1]):
        circuit.cx(quantum_register[i], quantum_register[j])


def exp_pauli(pauli,
        t,
        quantum_register=None,
        control_qubit=None,
        ):
    """
    The circuit for the exp(-i P t), where P is the Pauli term,
    t is the parameter.
    :param pauli: the string for the Pauli term: e.g. "XIXY".
    :param quantum_register: QuantumRegister.
    :param control_qubit: the control Qubit from QuantumRegister
                          other than quantum_register.
    :param t: the parameter t in exp(i P t).
    :return: QuantumCircuit that implements exp(i P t) or
             control version of it.
    """
    if quantum_register is None:
        quantum_register = QuantumRegister(len(pauli), 'q')
    elif len(pauli) != len(quantum_register):
        print(f'pauli: {pauli} len(quantum_register): {len(quantum_register)}')
        raise Exception("Pauli string doesn't match to the quantum register")

    pauli_circuit = QuantumCircuit(quantum_register)
    circuit_bracket = QuantumCircuit(quantum_register)
    pauli_ids = []

    # qiskit cpnvention!
    for i, p in enumerate(pauli[::-1]):
        if p == 'I':
            continue
        elif p == 'Z':
            pauli_ids.append(i)
        elif p == 'X':
            circuit_bracket.h(quantum_register[i])
            pauli_ids.append(i)
        elif p == 'Y':
            circuit_bracket.u(numpy.pi/2, numpy.pi/2, numpy.pi/2, quantum_register[i])
            pauli_ids.append(i)

    pauli_circuit += circuit_bracket
    exp_all_z(pauli_circuit,
            pauli_ids,
            t,
            quantum_register=quantum_register,
            control_qubit=control_qubit,
            )
    pauli_circuit += circuit_bracket

    return pauli_circuit


class paramCircuits:
    def __init__(self,
            nq,              # register length
            paulis=[],       # list of Pauli generator labels
            ref_state=None,  # inital state label, e.g., '001001'
            ref_circ=None,   # circuit for reference state
            ):
        self._nq = nq
        self._paulis = []
        self._thetas = []
        self.init_circ(ref_state, ref_circ, paulis)

    @property
    def paulis(self):
        return self._paulis

    def init_circ(self, ref_state, ref_circ, paulis):
        self._circ = QuantumCircuit(self._nq)
        self._qreg = self._circ.qregs[0]
        # temparary circuit to be appended
        self._tmpcirc = None
        # reference state
        if ref_state is not None:
            for i, s in enumerate(ref_state[::-1]):
                if s == '1':
                    self._circ.x(i)
        if ref_circ is not None:
            self._circ += ref_circ
        # intial generators
        for i, pauli in enumerate(paulis):
            self.expand(i, pauli)

    @property
    def qreg(self):
        if not hasattr(self, '_qreg'):
            self._qreg = self._circ.qregs[0]
        return self._qreg

    def expand(self, pauli):
        self._paulis.append(pauli)
        itheta = len(self._paulis)
        theta = Parameter(str(itheta))
        self._circ += exp_pauli(pauli, theta,
                # important for consistence
                quantum_register=self.qreg)
        self._thetas.append(theta)

    def get_circuit(self, params):
        assert(len(params) == len(self._thetas))
        binds = {th: val for th, val in zip(self._thetas, params)}
        if self._tmpcirc is None:
            circ = self._circ.bind_parameters(binds)
        else:
            circ = (self._circ + self._tmpcirc).bind_parameters(binds)
        return circ

    def set_tmpcirc(self, pauli):
        if self._tmpcirc is None:
            theta = Parameter(str(len(self._paulis)+1))
            self._thetas.append(theta)
        else:
            theta = self._thetas[-1]
        self._tmpcirc = exp_pauli(pauli, theta,
                # important for consistence
                quantum_register=self.qreg)

    def del_tmpcirc(self):
        if self._tmpcirc is not None:
            self._thetas.pop()
            self._tmpcirc = None


def egBandHamiltonianPartition(
        imp=0,
        path='../',
        ):
    with h5py.File(f'{path}/HEmbed.h5', 'r') as f:
        # take one spin block
        h1e = f[f'/impurity_{imp}/H1E'][::2, ::2].T
        d = f[f'/impurity_{imp}/D'][::2, ::2].T
        lam = f[f'/impurity_{imp}/LAMBDA'][::2, ::2].T
        v2e = f[f'/impurity_{imp}/V2E'][::2, ::2, ::2, ::2].T
    # one spin block
    n1 = h1e.shape[0]
    n2 = n1*2
    h1emb = numpy.zeros((n2, n2))
    h1emb[:n1, :n1] = h1e
    h1emb[:n1, n1:] = d.T
    h1emb[n1:, :n1] = d
    h1emb[n1:, n1:] = -lam

    # off-diagonal components
    h1_1 = numpy.zeros((2*n2, 2*n2))
    h1_1[:n2, :n2] = h1_1[n2:, n2:] = h1emb

    # density-density components
    h2_0 = numpy.zeros((2*n2, 2*n2, 2*n2, 2*n2))
    h2_1 = numpy.zeros((2*n2, 2*n2, 2*n2, 2*n2))
    for i, j, k, l in itertools.product(range(n1), repeat=4):
        if i == j and k == l:
            h2_0[i, j, k, l] = h2_0[i+n2, j+n2, k, l] = \
                h2_0[i, j, k+n2, l+n2] = \
                h2_0[i+n2, j+n2, k+n2, l+n2] = 0.5*v2e[i, j, k, l]
        else:
            h2_1[i, j, k, l] = h2_1[i+n2, j+n2, k+n2, l+n2] = \
                0.5*v2e[i, j, k, l]
            h2_1[i+n2, j+n2, k, l] = h2_1[i, j, k+n2, l+n2] = \
                0.5*v2e[i, j, k, l]

    # c+c+cc --> c+c c+c will introduce a shift in h1
    h2 = h2_0 + h2_1
    h1_1 -= numpy.einsum('ikkj->ij', h2)   # factor 1/2 already taken care of.
    # diagonal components
    h1_0 = numpy.diag(numpy.diag(h1_1))
    h1_1 -= h1_0

    return h1_0, h1_1, h2_0, h2_1


def get_fermion_nnop(
        h1=None,
        h2=None,
        nq=None,
        tol=1e-8,
        ):
    # get fermion operator in the density form.
    if nq is None:
        if h1 is not None:
            nq = len(h1)
        elif h2 is not None:
            nq = len(h2)
        else:
            raise ValueError('nq cannot be determined.')
    fop = 0
    if h1 is not None:
        inds = numpy.where(abs(h1) > tol)
        for i, j in zip(*inds):
            fop += FermionicOp((f'+_{i} -_{j}', h1[i, j]),
                    register_length=nq)
    if h2 is not None:
        inds = numpy.where(abs(h2) > tol)
        for i, j, k, l in zip(*inds):
            fop += FermionicOp((f'+_{i} -_{j} +_{k} -_{l}', h2[i, j, k, l]),
                    register_length=nq)
    return fop


def qubitOp(
        h1=None,
        h2=None,
        fop=None,
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):
    if fop is None:
        fop = get_fermion_nnop(h1=h1, h2=h2)
    if fop != 0:
        qubit_conv = QubitConverter(
                mapper,
                two_qubit_reduction=two_qubit_reduction,
                z2symmetry_reduction=z2symmetry_reduction,
                )
        qubit_op = qubit_conv.convert(fop,
                num_particles=fop.register_length//2,
                )
    else:
        qubit_op = None
    return qubit_op


@timeit
def LRF_2_body_OD_terms(h2, qregs):
    # For 2 body off-diagonal terms
    # constructing Low rank factorization circuits
    # Super matrix rep of four rank tensor (NxNxNxN)->(N^2xN^2)
    num_qubits = len(h2)
    N2 = num_qubits*num_qubits
    h2_pq_rs = numpy.reshape(h2, (N2, N2))
    # Diagonalize Super matrix
    w, v = numpy.linalg.eigh(h2_pq_rs)

    # Build Cholesky vectors
    Larr = [(v[:, i]*numpy.sqrt(wi)).reshape(num_qubits, num_qubits)
            for i, wi in enumerate(w) if wi > 1.e-6]
    # consistent check
    assert(numpy.allclose(h2, numpy.einsum('pij,pkl->ijkl', Larr, Larr)))

    qubit_h2 = qubitOp(h2=h2)

    res = []
    for L1 in Larr:
        circuit, rotqh2 = get_givens_rotns(L1, qregs, qop=qubit_h2)
        res.append((circuit, rotqh2))

    return res


def add_multiqubit_gate(pauli_string, param, circuit, qregs):
    num_qubits = len(pauli_string)
    if pauli_string == 'I'*num_qubits:
        gate = 1
        for j in range(len(pauli_string)):
            gate = numpy.kron(gate, Pauli('I').to_matrix())
        gate *= -1j * numpy.sin(param)
        gate += numpy.cos(param) * numpy.eye(2**num_qubits)
        circuit.unitary(gate, qregs, label=pauli_string)
    else:
        qubits_to_act_on = []
        gate = 1
        for j in range(len(pauli_string)):
            if pauli_string[j] != 'I':
                gate = numpy.kron(Pauli(pauli_string[j]).to_matrix(), gate)
                qubits_to_act_on.append(num_qubits-j-1)
        gate *= (-1j * numpy.sin(param))
        gate += numpy.cos(param) * numpy.eye(2**len(qubits_to_act_on))
        list_regs = [qregs[i] for i in qubits_to_act_on]
        label = pauli_string+"\n{:0.02f}".format(param.real)
        circuit.unitary(gate, list_regs, label=label)
    return circuit


@timeit
def get_givens_rotns(
        h,          # basis rotation which determines the given rotation
        qregs,      # quantum register
        qop=None,   # qubit operator to be rotated.
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):

    # get given roation
    w, v = numpy.linalg.eigh(h)
    givens = givensRotns(v.T)[0]

    circ = None
    op = None
    nrots = len(givens)
    for ir, rots in enumerate(givens):
        for tup in rots:
            i, j, theta, phi = tup
            # print('applying givens' +
            #         f' {ir}/{nrots} {i}, {j}, {theta:.3f}, {phi:.3f}')
            h1 = numpy.zeros_like(h)*1j
            h1[i, j] = -1j
            h1[j, i] = 1j
            qubit_h1 = qubitOp(
                    h1=h1,
                    mapper=mapper,
                    two_qubit_reduction=two_qubit_reduction,
                    z2symmetry_reduction=z2symmetry_reduction,
                    )
            for label, coef in qubit_h1.primitive.to_list():
                if qop is not None:
                    rop = PauliSumOp.from_list([
                            ('I'*qubit_h1.num_qubits, numpy.cos(theta*coef)),
                            (label, 1j*numpy.sin(theta*coef))
                            ])
                    if op is None:
                        op = rop
                    else:
                        op = (rop@op).reduce()
                if circ is None:
                    circ = QuantumCircuit(qregs)
                circ = add_multiqubit_gate(label, theta*coef, circ, qregs)
    if qop is not None:
        rqop = (op@qop).reduce()
        rqop = (rqop@op.adjoint()).reduce()
    else:
        rqop = None

    return circ, rqop


@timeit
def doubly_decomposed_form(h1_D, h1_X, h2_D, h2_X, nn_qop, qregs):
    # getting Cholesky vectors for two body off-diag terms
    circs_and_rotH_arr = LRF_2_body_OD_terms(h2_X, qregs)
    # getting the givens rotation circuit and the rotated Hamiltonian for the one body terms
    qubitH_1_X = qubitOp(h1=h1_X)
    circ_1_body, rotH_1_X = get_givens_rotns(h1_X, qregs, qop=qubitH_1_X)
    circs_and_rotH_arr.append((circ_1_body, rotH_1_X))
    qubitH_D = qubitOp(h1=h1_D, h2=h2_D) + nn_qop
    circs_and_rotH_arr.append((QuantumCircuit(qregs), qubitH_D))
    return circs_and_rotH_arr


def expectation(h, circ):
    state = execute(circ, Aer.get_backend('statevector_simulator'),
                    shots=1).result().get_statevector()
    h_expec = state.conj()@h.to_matrix()@state
    return h_expec


def hamiltonian_expectation_in_doubly_decomposed_form_sv(params,
        ansatz,
        circs_and_rotH,
        qh_chk=None,
        ):
    ansatz_circ = ansatz.get_circuit(params)
    res = 0
    for circ, roth in circs_and_rotH:
        res += expectation(roth, ansatz_circ + circ).real
    # print(f"energy (doubly decomposed): {res:.6f}")
    # consistent check
    if qh_chk is not None:
        h_sum_direct = expectation(qh_chk, ansatz_circ).real
        assert(abs(res-h_sum_direct) < 1.e-6)
    return res


@timeit
def hamiltonian_estimation_in_doubly_decomposed_form_qasm(
        ansatz,
        params,
        circs_and_rotH,
        shots=2**10,
        comm=None,
        ):

    def Energy_Estimator_DD(circ, Op, shots, comm=None):
        def getCounts(circ, shots):
            circ.add_register(ClassicalRegister(circ.num_qubits, 'c'))
            clist = list(range(circ.num_qubits))
            circ.measure(clist, clist)
            job_sim = execute(circ,
                    AerSimulator(
                        method='density_matrix',
                        ),
                    shots=shots,
                    )
            res = job_sim.result()
            counts = res.get_counts(circ)
            return counts

        def estimate(PauliOp,
                counts,
                comm=None,
                ):
            PauliString, coeff = PauliOp.primitive.to_list()[0]
            indexOfZs = numpy.where(numpy.array(list(PauliString)) == 'Z')[0]
            bitstrings, cnts = list(counts.keys()), list(counts.values())
            p = 0
            for i in range(len(bitstrings)):
                counter_1 = list(numpy.array(list(bitstrings[i]))[
                                 indexOfZs]).count('1')
                if counter_1 % 2 == 1:
                    p = p+cnts[i]

            sum_cnts = sum(cnts)
            if comm is not None:
                sum_cnts = comm.allreduce(sum_cnts, op=MPI.SUM)
                p = comm.allreduce(p, op=MPI.SUM)
            p /= sum_cnts
            mean = (1-2*p)
            return mean*coeff.real

        def meanOp(PauliOp, counts, comm=None):
            mean = 0
            for i in range(len(PauliOp)):
                mean = mean+estimate(PauliOp[i], counts, comm=comm,)
            return mean

        counts_Arr = getCounts(circ, shots)
        m1 = meanOp(Op, counts_Arr, comm=comm)
        return m1

    if comm is not None:
        size = comm.Get_size()
        shots = shots//size + 1
        if comm.Get_rank() == 0:
            print(f'shots = {shots}')

    ansatz_circ = ansatz.get_circuit(params)
    results = [ Energy_Estimator_DD(
            ansatz_circ+circh[0],
            circh[1],
            shots,
            comm=comm,
            )
            for circh in circs_and_rotH ]

    return sum(results)


def Energy(
        params,
        ansatz,
        Hmat,
        ):
    circ = ansatz.get_circuit(params)
    state = execute(circ, Aer.get_backend('statevector_simulator'),
            shots=1).result().get_statevector()
    E = (state.conj()@Hmat@state).real
    return E


def chk_fidelity(params,
        ansatz,
        Hmat,
        ):
    circ = ansatz.get_circuit(params)
    state = execute(circ, Aer.get_backend('statevector_simulator'),
            shots=1).result().get_statevector()

    w, v = numpy.linalg.eigh(Hmat)
    res = (abs(v[:,0].dot(state)))**2
    e = state.dot(Hmat).dot(state.conj())
    print(f'fidelity: {res.real:.6f}')
    print(f'energy: {e:.6f} vs e_gs: {w[0]:.6f}')


def Energy_sv(ansatz, params, Hmat, shots=1):
    return Energy(params,
            ansatz,
            Hmat,
            )

def commutatorPool(qubitH,
        rank,
        remove_z=True,
        ):
    if os.path.isfile('../incar'):
        incar = json.load(open("../incar", "r"))
        labels = incar["pool_commutator"]
        if ranl == 0:
            print('pool read in from incar.')
    else:
        labels = []
        for i, op1 in enumerate(qubitH):
            for op2 in qubitH[i+1:]:
                op12 = ((op1@op2).reduce() - (op2@op1).reduce()).reduce()
                assert(len(op12) == 1)
                op12 = op12.to_pauli_op()
                # remove coefficient
                op12._coeff = 1
                label = str(op12)
                if remove_z:
                    label = label.replace('Z', 'I')
                if label in labels or label.count('Y')%2 == 0:
                    continue
                labels.append(label)

    pool = [PauliOp(Pauli(label)) for label in labels]
    if rank == 0:
        print(f'pool dim: {len(pool)}')
        print(f"pool: {labels}")
    return pool

def compute_gradient_sv(M, state):
    # -1j <HP - PH> for |\Psi> = e^{-iPt} |0>.
    grad = state@M@numpy.conjugate(state)
    grad *= -1j
    return grad.real


def compute_gradient_sv2(ansatz,
        op,
        params,
        circs_and_rotH,
        qh_chk=None,
        ):
    ansatz.set_tmpcirc(str(op))
    E1 = hamiltonian_expectation_in_doubly_decomposed_form_sv(
            params + [numpy.pi/4],
            ansatz,
            circs_and_rotH,
            qh_chk=qh_chk,
            )
    E2 = hamiltonian_expectation_in_doubly_decomposed_form_sv(
            params + [-numpy.pi/4],
            ansatz,
            circs_and_rotH,
            qh_chk=qh_chk,
            )
    gradient = E1-E2
    return gradient


def compute_gradient(ansatz,
        op,
        params,
        circs_and_rotH,
        shots=2**10,
        ):
    ansatz.set_tmpcirc(str(op))
    E1 = hamiltonian_estimation_in_doubly_decomposed_form_qasm(
            ansatz,
            params + [numpy.pi/4.],
            circs_and_rotH,
            shots=shots,
            )
    E2 = hamiltonian_estimation_in_doubly_decomposed_form_qasm(
            ansatz,
            params+[-numpy.pi/4.],
            circs_and_rotH,
            shots=shots,
            )
    gradient = E1-E2
    return gradient


def fsmo(x, a, b, c):
    return a*numpy.cos(2*x - b) + c


def SMO2_qasm(cost,
        ansatz,
        params,
        circs_and_rotH=None,
        runs=40,
        shots=2**10,
        comm=None,
        ):

    def E_landscape(ind, ang, cost, ansatz, params, comm=None,):
        params1 = copy.deepcopy(params)
        params1[ind] = ang
        data = cost(ansatz,
                params1,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                comm=comm,
                )
        return data

    def determine_unknowns(E, cost, ansatz, params, ind, comm=None, nmesh=4):
        # eight grids
        thetas = []
        vals = numpy.zeros(nmesh*2)
        for i in range(-nmesh, nmesh):
            theta = i*numpy.pi/2/nmesh
            iv = i + nmesh
            thetas.append(theta)
            if comm is None or iv%comm.Get_size() == comm.Get_rank():
                vals[iv] = E_landscape(ind, theta, cost, ansatz, params)

        if comm is not None:
            vals = comm.allreduce(vals, op=MPI.SUM)

        # initial guess
        L1, L2, L3 = vals[4], vals[6], vals[2]
        ratio = (L3-L2)/(2*L1-L2-L3)
        a3 = (L2+L3)/2.
        a2 = -numpy.arctan(ratio)
        a1 = (L1-a3)/numpy.cos(numpy.arctan(ratio))
        # fitting
        from scipy.optimize import curve_fit
        popt, pcov = curve_fit(fsmo, thetas, vals, p0=[a1, a2, a3])
        err = sum([abs(val - fsmo(th, *popt)) for val, th in zip(vals, thetas)])
        err /= len(thetas)
        if comm is None or comm.Get_rank() == 0:
            print(f'fitting err: {err:.6f}')
        return popt

    def update(E, cost, ansatz, params, ind, comm=None,):
        a1, a2, a3 = determine_unknowns(E, cost, ansatz, params, ind, comm=comm,)
        thetaStar = a2/2.+numpy.pi/2. if a1 > 0 else a2/2.
        newParams = copy.deepcopy(params)
        newParams[ind] = thetaStar
        # for low shots, this can be off.
        updEnergy_fit = fsmo(thetaStar, a1, a2, a3)
        updEnergy = cost(ansatz,
                newParams,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                comm=comm,
                )
        if comm is None or comm.Get_rank() == 0:
            print(f'upd energy: fit={updEnergy_fit:.6f} calc={updEnergy:.6f}')
        return newParams, updEnergy


    # current energy
    Eold = E = cost(ansatz,
            params,
            circs_and_rotH=circs_and_rotH,
            shots=shots,
            comm=comm,
            )
    # reverse order
    inds = numpy.arange(len(params))[::-1]

    for iloop in range(runs):
        for ind in inds:
            params, E = update(E, cost, ansatz, params, ind, comm=comm,)
        conv_err = Eold - E
        if comm is None or comm.Get_rank() == 0:
            print("looped "+str(iloop)+" times")
            print("loop energy", E)
            print("inner loop error", conv_err)
        Eold = E
    res = {'x': params, 'fun': E}
    return res


@timeit
def run(
        shots=2**14,
        shots_g=2**16,
        chem_pot=-9.778,
        steps=40,
        maxrun=80,
        remove_z=True,
        ):
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    h1_D, h1_X, h2_D, h2_X = egBandHamiltonianPartition()
    qubitH = qubitOp(h1=h1_D+h1_X,
            h2=h2_D+h2_X,
            )

    # commutator pool
    commutator_pool = commutatorPool(qubitH,
            rank,
            remove_z=remove_z,
            )

    # penalty term, disadvantageous as it increase the energy range of H
    # weight = 10.
    # ne = len(h1_D)//2
    # nn_fop = get_fermion_nnop(h1=numpy.eye(h1_D.shape[0]))
    # nn_fop = (nn_fop - FermionicOp(('', ne))).reduce()
    # nn_fop = (nn_fop@nn_fop).reduce()
    # nn_qop = qubitOp(fop=nn_fop)
    # add_qop = nn_qop = weight*nn_qop
    # qubitH += nn_qop

    # alternatively, chemical potential
    n_fop = get_fermion_nnop(h1=numpy.eye(h1_D.shape[0]))
    n_qop = qubitOp(fop=n_fop)
    add_qop = n_qop = chem_pot*n_qop
    qubitH += n_qop
    qubitH.reduce()

    Hmat = qubitH.to_matrix().real
    w, v = numpy.linalg.eigh(Hmat)
    Eg = w[0]
    if rank == 0:
        print(f'shots = {shots} shots_g = {shots_g}')
        print(f'Eg = {Eg:.6f}')

    n_op = qubitOp(h1=numpy.eye(h1_D.shape[0]))
    if rank == 0:
        print(f'n fermion: {v[:,0].dot(n_op.to_matrix()).dot(v[:,0].conj()):.4f}')

    # constructing matrices for gradient computation
    MatrixRepOfPoolOps = [(qubitH@op - op@qubitH).reduce().to_matrix()
            for op in commutator_pool]

    if os.path.isfile('ansatz.pkle'):
        if rank == 0:
            with open('ansatz.pkle', 'rb') as f:
                data = pickle.load(f)
        else:
            data = None
        data = comm.bcast(data, root=0)
        ansatz, params = data
        ansatz.del_tmpcirc()
    else:
        ansatz = paramCircuits(
                qubitH.num_qubits,
                ref_state='001001',
                )
        params = []

    # low rank factorization
    circs_and_rotH = doubly_decomposed_form(h1_D, h1_X, h2_D, h2_X, add_qop,
            ansatz.qreg)

    if rank == 0:
        chk_fidelity(params, ansatz, Hmat)

    ti = time.time()

    for istep in range(len(ansatz.paulis), steps):
        # state-vector
        # circ = ansatz.get_circuit(params)
        # state = execute(circ,
        #         Aer.get_backend('statevector_simulator'),
        #         shots=1).result().get_statevector()
        # grads=numpy.array([
        #         compute_gradient_sv(MatrixRepOfPoolOps[i], state)
        #         for i in range(len(commutator_pool)) ])

        # grads=numpy.array(Parallel(n_jobs=1,verbose=0) \
        #         (delayed(compute_gradient_sv2) \
        #         (ansatz, op, params, qubitH, circs_and_rotH) \
        #         for op in commutator_pool))
        # assert(numpy.allclose(grads1, grads))

        # qasm
        grads = numpy.zeros(len(commutator_pool))
        for i, op in enumerate(commutator_pool):
            if i%size != rank:
                continue
            grads[i] = compute_gradient(
                    ansatz,
                    op,
                    params,
                    circs_and_rotH,
                    shots=shots_g,
                    )
        ansatz.del_tmpcirc()
        grads = comm.allreduce(grads, op=MPI.SUM)

        indexes = numpy.argsort(abs(grads))[::-1]
        if rank == 0:
            print("highest grads:",
                    [numpy.round(grads[i], decimals=4) for i in indexes])

        if len(ansatz.paulis) > 0:
            if (str(commutator_pool[indexes[0]]) != ansatz.paulis[-1]):
                PauliOp = commutator_pool[indexes[0]]
            else:
                PauliOp = commutator_pool[indexes[1]]
        else:
            PauliOp = commutator_pool[indexes[0]]

        if rank == 0:
            print(f"i: {istep} chosen generator {PauliOp}")
        params.append(0.0)
        ansatz.expand(str(PauliOp))

        # state-vector
        # res = scipy.optimize.minimize(Energy, params, args=(ansatz), method='BFGS',
        #         tol=1e-4)

        runs = min(maxrun, 1+istep*10)
        res = SMO2_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,
                 ansatz,
                 params,
                 circs_and_rotH=circs_and_rotH,
                 runs=runs,
                 shots=shots,
                 comm=comm,
                 )

        params, E = list(res['x']), res['fun']

        if rank == 0:
            # save intermediate results.
            with open(f'ansatz_{istep}.pkle', 'wb') as f:
                pickle.dump([ansatz, params], f)

            print("num_params", len(params))
            print("Energy", E)
            e_sv = Energy_sv(ansatz, params, Hmat)
            print(f'iter {istep} Energy_sv = {e_sv:.6f}')
            print("time elapsed", time.time()-ti)



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--shots", type=int, default=10,
            help="shots power. dflt: 10.")
    parser.add_argument("-g", "--shotsg", type=int, default=16,
            help="shots power for gradient. dflt: 16.")
    parser.add_argument("-n", "--nsteps", type=int, default=40,
            help="number of iterations. dflt: 40.")
    parser.add_argument("-m", "--msweeps", type=int, default=40,
            help="number of iterations. dflt: 40.")
    args = parser.parse_args()
    run(shots=2**args.shots,
            shots_g=2**args.shotsg,
            steps=args.nsteps,
            maxrun=args.msweeps,
            )
