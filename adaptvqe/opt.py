import numpy, itertools
from scipy.optimize import minimize


def seq_batch_opt(fun,
        x0,
        order=1,
        args=(),
        maxiter=80,
        tol=1e-4,
        ):
    x0 = numpy.asarray(x0)
    np = len(x0)
    if order > np:
        order = np
    elif order < 1:
        order = 1
    print(f"seq_batch_opt with order = {order}")
    # precalculate arrays
    # sin-cos basis
    alist = [get_amat(order)]
    if np%order > 0:
        alist.append(get_amat(np%order))

    eold = 1.e10
    # potentially optimize the new added parameter first.
    indices = numpy.arange(np)[::-1]
    for iter in range(maxiter):
        for i in range(0, np, order):
            nth = min(order, np-i)
            if nth == order:
                amat = alist[0]
            else:
                amat = alist[1]
            bvec = numpy.zeros(amat.shape[0])
            theta_list = numpy.linspace(-numpy.pi, numpy.pi,
                    num=3, endpoint=False)
            x1 = x0.copy()
            for j, ths in enumerate(itertools.product(theta_list, repeat=nth)):
                # parameter point
                x1[indices[i:i+nth]] = ths
                bvec[j] = fun(x1, *args)
            res = numpy.linalg.lstsq(amat, bvec, rcond=1.e-8)
            coefs = res[0]
            # print(f'solving ax=b rank: {res[2]}')
            x0s = x0[indices[i:i+nth]]
            ret = minimize(_cost_fun, x0s, args=(coefs, nth), method='BFGS',
                    # jac=_jac_fun,
                    )
            x0[indices[i:i+nth]] = ret['x']
            e0 = ret['fun']
        print(f'iter {iter} e0: {e0:.9f} reduced: {e0-eold:.2e}')
        # fix numbe of iteration for now.
        # if eold - e0 < tol and iter > 5:
        #     break
        # else:
        #     eold = e0

        eold = e0

        # continuous sweep
        indices = numpy.arange(np)[::-1]
        # random sweep
        # numpy.random.shuffle(indices) # not effective

    return x0.tolist(), e0


def _cost_fun(x, coefs, nth):
    ss, cs = numpy.sin(x/2), numpy.cos(x/2)
    res = 0
    for coef, ps in zip(coefs, itertools.product(range(3), repeat=nth)):
        res += coef*numpy.prod([s**p*c**(2-p) for s, c, p in zip(ss, cs, ps)])
    return res


def _jac_fun(x, coefs, nth):
    '''still sume bug
    '''
    nx = len(x)
    ss, cs = numpy.sin(x/2), numpy.cos(x/2)
    jac = numpy.zeros(nx)
    for coef, ps in zip(coefs, itertools.product(range(3), repeat=nth)):
        tmp = numpy.ones((nx, 2))
        for i, s, c, p in zip(itertools.count(), ss, cs, ps):
            for j in range(nx):
                if i == j:
                    if p > 0:
                        tmp[j, 0] *= p*s**(p-1)*c/2.*c**(2-p)
                    else:
                        tmp[j, 0] *= 0
                    if p < 2:
                        tmp[j, 1] *= s**p*(2-p)*c**(1-p)*(-s)/2.
                    else:
                        tmp[j, 1] *= 0
                else:
                    tmp[j, :] *= s**p*c**(2-p)
        jac -= coef*numpy.sum(tmp, axis=1)
    return jac


def get_amat(order):
    a = numpy.ones((3**order, 3**order))
    theta_list = numpy.linspace(-numpy.pi, numpy.pi, num=3, endpoint=False)
    sincos_list = [(numpy.sin(th/2), numpy.cos(th/2)) for th in theta_list]
    for i, scs in enumerate(itertools.product(sincos_list, repeat=order)):
        # sin-cos pairs
        for j, ps in enumerate(itertools.product(range(3), repeat=order)):
            # exponentials
            for (s, c), p in zip(scs, ps):
                a[i, j] *= s**p*c**(2-p)
    return a


def adagradient(fun,
        x0,
        jac,
        rho=0.9,
        eps=1e-5,
        args=(),
        maxiter=40,
        ):
    scale = 1
    x0 = numpy.asarray(x0)
    emin = fun(x0, *args)
    for iloop in range(maxiter):
        grad = jac(x0, *args)
        x1 = x0 - scale*grad
        e0 = fun(x1, *args)
        while e0 >= emin and scale >= 0.005:
            scale /= 2
            x1 = x0 - scale*grad
            e0 = fun(x1, *args)

        if scale < 0.005:
            break
        print(f'i-adadelta: {iloop} e: {e0:.6f}')
        emin = e0
        x0 = x1

    print(f'final emin: {emin:.6f}')
    return x0.tolist(), emin


def adadelta(fun,
        x0,
        jac,
        rho=0.8,
        beta=0.9,
        eps=1e-9,
        args=(),
        maxiter=400,
        lr0=0.015,      # final learning rate
        lrmax=1e5,
        nstepc=37,
        tol=1e-6,
        ):
    eps = numpy.ones_like(x0)*eps
    x0 = numpy.asarray(x0)
    # leaky average of the second moment of gradient
    s = numpy.zeros_like(x0)
    # leaky averaqe of the second momnet of change of parameters
    delta = numpy.zeros_like(x0)
    eold = fun(x0, *args)
    err = 1

    for iloop in range(maxiter):
        # learning rate bound
        scale = numpy.exp(-nstepc/maxiter*iloop)
        lr_u = lr0 + lrmax*scale
        lr_d = lr0*(1-scale)

        grad = jac(x0, *args)
        # update the array
        s[:] = beta*s + (1-beta)*numpy.square(grad)
        lr = numpy.sqrt(delta + eps)/numpy.sqrt(s + eps)  # learning rate
        # check bound
        for i, lri in enumerate(lr):
            if lri > lr_u:
                eps[i] = lr_u*lr_u*s[i]
                lr[i] = lr_u
                delta[i] = 0
            elif lri < lr_d:
                eps[i] = lr_d*lr_d*s[i]
                lr[i] = lr_d
                delta[i] = 0
        g = lr*grad
        x0 -= g
        delta[:] = beta*delta + (1-beta)*g*g

        e0 = fun(x0, *args)
        err = rho*err + (1-rho)*abs(e0 - eold)
        print(f'i-adadelta: {iloop} e: {e0:.6f} err: {eold-e0:.6f}' +
                f" merr: {err:.6f}")
        eold = e0
        if err < tol:
            break

    print(f'final emin: {e0:.6f}')
    return x0.tolist(), e0
