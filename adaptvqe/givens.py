# by Anirban Mukherjee (amukh1290@gmail.com) and Yongxin Yao (yxphysics@gmail.com)

import numpy
from openfermion.linalg import givens_decomposition_square
from qiskit_nature.converters.second_quantization import QubitConverter
from qiskit_nature.problems.second_quantization.electronic.builders import \
        fermionic_op_builder
from qiskit_nature.mappers.second_quantization import ParityMapper, \
        JordanWignerMapper


def get_options():
    try:
        vglobal = {}
        vlocal = {}
        exec(open("cygutz.conf", "r").read(), vglobal, vlocal)
        return vlocal["conf"]
    except:
        return {}


def get_qubit_op(h1,
        h2=None,
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):
    num_qubits = h1.shape[0]
    fer_op = fermionic_op_builder.build_ferm_op_from_ints(h1, h2)
    qubit_op = mapper.map(fer_op)
    qubit_conv = QubitConverter(mapper,
            two_qubit_reduction=two_qubit_reduction,
            z2symmetry_reduction=z2symmetry_reduction,
            )
    qubit_op = qubit_conv.convert(fer_op, num_qubits//2)
    return qubit_op


def get_givens_labels(u,
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):
    '''given unitary transformation in single particle basis,
    u_{i, a} = <\phi^{new}_i | \phi^{old}_a>,
    get the givens rotation in terms of Pauli ratotaions and angles.
    assume spin symmetry, and u is for one spin block.
    '''
    decomposition, _ = givens_decomposition_square(u)
    norb = u.shape[0]
    norb2 = norb*2
    h1 = numpy.zeros((norb2, norb2))

    # angle and genetator list
    gens_list = []
    for elem in decomposition:
        for params in elem:
            i, j, theta, phi = params
            # spin block
            for ispin in range(2):
                h1 *= 0
                h1[i+ispin*norb, j+ispin*norb] = 1
                h1[j+ispin*norb, i+ispin*norb] = -1
                qubit_h1 = get_qubit_op(h1,
                        mapper=mapper,
                        two_qubit_reduction=two_qubit_reduction,
                        z2symmetry_reduction=z2symmetry_reduction,
                        )
                # get the theta factor and remove 1j
                qubit_h1 *= theta/1.j
                gens_list += [f"{w.real:16.12f}*{p}"
                        for p, w in qubit_h1.primitive.to_list()]
    return gens_list


if __name__ == "__main__":
    n = 4
    # get symmetric matrix
    a = numpy.random.rand(n, n)
    a += a.T
    w, v = numpy.linalg.eigh(a)
    gens_list = get_givens_labels(v.T,
            mapper=JordanWignerMapper(),
            two_qubit_reduction=False,
            )
    print(gens_list)
