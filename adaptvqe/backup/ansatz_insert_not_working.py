# author: Yongxin Yao (yxphysice@gmail.com)
import numpy, h5py
import scipy.optimize
from scipy.sparse.linalg import expm_multiply
from timing import timeit


class ansatz:
    '''
    define the main procedures of adapt-vqe calculation.
    set up: 1) default referece state of the ansatz.
            2) operator pool
    '''
    def __init__(self,
            model,           # the qubit model
            opspm=True,      # operator in scipy sparse matrix
            ):
        # variational parameters.
        self._params = []
        # list of unitaries in terms of operators and labels
        self._ansatz = [[], []]
        self._opspm = opspm
        self._model = model
        self._nq = model._nsite

        # set reference state
        self.set_ref_state()
        self._state = None
        # generate operator
        self.setup_pool()

    @timeit
    def additive_optimize(self):
        # change the representation of H if necessary.
        if self._opspm:
            self._h = self._model._h.data.tocsr()
        else:
            self._h = self._model._h
        # (adaptively) optimize the ansatz to represent the groud state
        # of the model Hamiltonian.
        self.optimize_params()
        print(f"initial cost = {self._cost:.6f}")
        iloop = 0
        while True:
            # one iteration of qubit-adapt-vqe.
            added = self.add_op()
            if not added:
                # reaching convergence.
                break
            self.optimize_params()
            self.update_state()
            print(f"ngates: {self._ngates}")
            print(f"iter {iloop}: cost = {self._cost:.6f}")
            iloop += 1

    @property
    def state(self):
        return self._state

    @property
    def ngates(self):
        return self._ngates[:]

    def get_cost(self):
        '''
        get the value of the cost function (Hamiltonian expectation value).
        '''
        # have the state vector updated.
        self.update_state()
        # expectation value of the Hamiltonian
        if self._opspm:
            res = self._state.conj().dot(self._h.dot(self._state))
        else:
            res = self._h.matrix_element(self._state, self._state)
        return res.real

    @timeit
    def update_state(self):
        self._state = self.get_state()

    def setup_pool(self):
        '''
        setup Hamiltonian pool from incar.
        '''
        labels = self._model._incar["hao"]
        labels = [s.split('*')[-1] for s in labels]
        one = 'I'*self._nq
        self._pool = [[self._model.label2op(s), s] for s in labels
                if s != one]
        self._ngates = [0]*len(self._pool[0][1])
        print(f'pool dimension: {len(self._pool)}')

        if self._opspm:
            # convert to more efficient data structure if needed.
            for i, op in enumerate(self._pool):
                self._pool[i][0] = op[0].data.tocsr()

    def set_ref_state(self):
        '''
        default is the ground state of h1 in the correct valence sector.
        '''
        h = self._model.coeflabels2op(self._model._incar['h1ao'])
        npenalty = self._model.get_npenalty()
        h += npenalty
        w, v = h.groundstate()
        print(f"<n_penalty>: {abs(npenalty.matrix_element(v, v)):.2e}")
        if self._opspm:
            self._ref_state = v.full().reshape(-1)
        else:
            self._ref_state = v
        # add one-layer of simplified hva
        one = 'I'*self._nq
        for labels in self._model._incar['op_ansatz']:
            # get the first non identity label
            for label in labels:
                label = label.split('*')[1]
                if label != one:
                    break
            op = self._model.label2op(label)
            if self._opspm:
                op = op.data.tocsr()
            self._ansatz[0].append(op)
            self._ansatz[1].append(label)
            self._params.append(numpy.pi/7)

    @timeit
    def optimize_params(self):
        if len(self._params) > 0:
            # full reoptimization of the ansatz given the initial point.
            res = scipy.optimize.minimize(fun_cost,
                    self._params,               # starting parameter point
                    args=(self._ansatz[0],      # only need the ansatz
                                                # operators, not the indices.
                            self._ref_state,    # reference state
                            self._h,            # Hamiltonian
                            ),
                    method='BFGS',              # optimizer
                    jac=fun_jac,                # analytical jacobian function
                    )
            if res.success:
                # save parameters and cost.
                self._params = res.x.tolist()
                self._cost = res.fun
            else:
                print(res.message)
        else:
            # no parameters to optimize, directly evaluate.
            self._cost = self.get_cost()

    @timeit
    def add_op(self,
            tol=1.e-6,
            ):
        '''
        adding a initary  in the adapt-vqe.
        '''
        scores = self.get_pool_scores()
        # relative energy reduction
        scores[0] -= self._cost
        ids = numpy.argsort(scores[0])
        iadd = ids[0]
        print("top 3 scores: "+ \
                f"{' '.join(f'{scores[0][i]:.2e}' for i in ids[:3])}")
        if len(self._ansatz[1]) > 0  \
                and self._pool[iadd][1] == self._ansatz[1][0]:
            # no further improvement
            print(f"abort: pauli ops {self._ansatz[1][0]}" + \
                    f" vs {self._pool[iadd][1]}")
            return False
        elif abs(scores[0][iadd]) < tol:
            print(f"converge: gradient = {scores[0][iadd]:.2e} too small.")
            return False
        else:
            self._ansatz[0].insert(0, self._pool[iadd][0])
            # label
            self._ansatz[1].insert(0, self._pool[iadd][1])
            self.update_ngates()
            print(f"op {self._ansatz[1][0]} inserted.")
            self._params.insert(0, scores[1][iadd])
            return True

    def update_ngates(self):
        '''
        update gate counts.
        '''
        label = self._ansatz[1][-1]
        iorder = len(label) - label.count('I')
        self._ngates[iorder-1] += 1

    def get_state(self):
        return get_ansatz_state(self._params,
                        self._ansatz[0],
                        self._ref_state,
                        )

    def get_pool_scores(self):
        '''intead of gradient at 0, use 3-point 1d mesh evaluation.
        Unitary is inserted in the beginning, rather than appended in the end.
        '''
        scores = [[], []]
        for op in self._pool:
            op = op[0]
            res = self.get_1dmin(op)
            scores[0].append(res['fun'].real)
            scores[1].append(res['x'])
        return numpy.array(scores)

    def get_1dmin(self, op):
        # op is single pauli string
        xlist = numpy.pi/3*numpy.arange(3)*2
        ylist = []
        ansatz = [op] + self._ansatz[0]
        for x in xlist:
            params = [x] + self._params
            res = fun_cost(params, ansatz, self._ref_state, self._h)
            ylist.append(res)
        coefs=numpy.fft.rfft(ylist)
        maxerr = numpy.max(numpy.abs(ylist-funfourier(xlist, coefs)))

        # inspectation
        # import matplotlib.pyplot as plt
        # xdata = numpy.linspace(0, 2*numpy.pi, 20)
        # plt.plot(xlist, ylist, "o")
        # plt.plot(xdata, funfourier(xdata, coefs))
        # ydata2 = []
        # for x in xdata:
        #     params = [x] + self._params
        #     ydata2.append(fun_cost(params, ansatz, self._ref_state, self._h))
        # plt.plot(xdata, ydata2, "+")
        # plt.show()

        if maxerr > 1.e-10:
            print(f"max diff in fitting: {maxerr:.2e}")
        res = scipy.optimize.minimize_scalar(funfourier,
                bounds=[-numpy.pi, numpy.pi],
                args=tuple([coefs]),
                method='Bounded',
                )
        return res

    def save_ansatz(self):
        with h5py.File("ansatz.h5", "w") as f:
            # initial state params
            f["/params"] = self._params_init
            # ansatz operator labels
            f["/ansatz_code"] = self._ansatz[1]
            # ngates
            f["/ngates"] = self._ngates
            # reference state
            f["/ref_state"] = self._ref_state

    def save_state(self, t):
        with h5py.File("state.h5", "w") as f:
            f["t"] = t
            if self._opspm:
                f["state"] = self._state
            else:
                f["state"] = self._statevec.full().reshape(-1)


def funfourier(x, coefs):
    res = coefs[0]
    n = len(coefs)
    for i, coef in enumerate(coefs[1:]):
        res += 2*numpy.real(coef*numpy.exp(1j/(n-1)*(i+1)*x))

    res /= 2*n-1
    return res.real


def fun_cost(params, ansatz, ref_state, h):
    state = get_ansatz_state(params, ansatz, ref_state)
    if isinstance(state, numpy.ndarray):
        res = numpy.vdot(state, h.dot(state))
    else:
        res = h.matrix_element(state, state)
    return res.real


def fun_jac(params, ansatz, ref_state, h):
    # - d <var|h|var> / d theta
    np = len(ansatz)
    vec = get_ansatz_state(params, ansatz, ref_state)
    opspm = isinstance(vec, numpy.ndarray)
    # <vec|h
    if opspm:
        h_vec = h.dot(vec)
    else:
        h_vec = h*vec

    jac = []
    state_i = ref_state
    if opspm:
        for i in range(np):
            op = ansatz[i]
            state_i = expm_multiply(-0.5j*params[i]*op, state_i)
            state = op.dot(state_i)
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                state = expm_multiply(-0.5j*theta*op, state)
            zes = numpy.vdot(h_vec, state)
            jac.append(zes.imag)
    else:
        for i in range(np):
            opth = -0.5j*params[i]*ansatz[i]
            state_i = opth.expm()*state_i
            state = op*state_i
            for theta, op in zip(params[i+1:], ansatz[i+1:]):
                opth = -0.5j*theta*op
                state = opth.expm()*state
            zes = h_vec.overlap(state)
            jac.append(zes.imag)
    res = numpy.array(jac)
    return res


def get_ansatz_state(params, ansatz, ref_state):
    state = ref_state
    opspm = isinstance(state, numpy.ndarray)
    for theta, op in zip(params, ansatz):
        if opspm:
            state = expm_multiply(-0.5j*theta*op, state)
        else:
            opth = -0.5j*theta*op
            state = opth.expm()*state
    return state
