#!/usr/bin/env python
# coding: utf-8

# In[173]:


def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
import numpy
import copy, time
import qiskit
from qiskit_nature.converters.second_quantization import QubitConverter
from qiskit.providers.aer import AerSimulator
from openfermion.linalg import givens_decomposition_square as givensRotns
from qiskit.opflow import I,X,Y,Z,StateFn,AbelianGrouper
from qiskit.opflow.primitive_ops import PauliSumOp
from qiskit_nature.mappers.second_quantization import JordanWignerMapper,ParityMapper
from qiskit_nature.problems.second_quantization.electronic.builders import fermionic_op_builder
from qiskit.quantum_info import Pauli
from qiskit.chemistry import FermionicOperator
from qiskit.aqua.operators.legacy import op_converter
from openfermion.circuits import slater_determinant_preparation_circuit
from qiskit import QuantumCircuit,execute,QuantumRegister,ClassicalRegister
from qiskit import Aer
from qiskit.circuit.random import random_circuit
from joblib import Parallel,delayed
from qiskit.aqua.operators import WeightedPauliOperator,Z2Symmetries
from qiskit.chemistry.components.variational_forms import UCCSD
import scipy


# In[174]:


def countYgates(pauli_label):
    countYgates = sum(map(lambda x : 1 if 'Y' in x else 0, pauli_label))
    return countYgates
def egBandHamiltonianPartition(U):
    #Getting chemical Potential for Half-Filling
    with open('./chem_pot_for_Half_Fill.txt','r') as f:
        lines=f.readlines()[1:]
        for line in lines:
            elems=line.split()
            if int(elems[0])==U:
                muHalf=float(elems[1]) #Chem Pot for a given Hubbard U
    #Getting the one body and two body interaction vertexes
    with open('./v1e.dat','r') as f:
            lines=f.readlines()[1:]
            num_sites=4
            chem_pot=numpy.zeros((2*num_sites,2*num_sites))
            eg_h1_0=numpy.zeros((2*num_sites,2*num_sites))
            eg_h1_1=numpy.zeros((2*num_sites,2*num_sites))
            for line in lines:
                line=line.split()
                i,j=map(int,line[:2])
                val=float(line[2])
                if(i==j):
                    eg_h1_0[i,j]=eg_h1_0[i+num_sites,j+num_sites]=val
                else:
                    eg_h1_1[i,j]=eg_h1_1[i+num_sites,j+num_sites]=val
            for i in range(2*num_sites):
                chem_pot[i][i]=-muHalf
            eg_h1_0=eg_h1_0+chem_pot
    with open('./v2e.dat','r') as f:
        num_sites=4
        eg_h2_0=numpy.zeros((2*num_sites,2*num_sites,2*num_sites,2*num_sites))
        eg_h2_1=numpy.zeros((2*num_sites,2*num_sites,2*num_sites,2*num_sites))
        for line in f:
            if "#" in line:
                continue
            line = line.split()
            i,j,k,l = map(int, line[:4])
            val = float(line[4])
            if((i==j) and (k==l)):
                eg_h2_0[i,j,k,l] = eg_h2_0[i+num_sites,j+num_sites,k,l] = eg_h2_0[i,j,k+num_sites,l+num_sites]                 = eg_h2_0[i+num_sites,j+num_sites,k+num_sites,l+num_sites] = 0.5*val  # convention with 0.5 factor included.
            else:
                eg_h2_0[i,j,k,l] = eg_h2_0[i+num_sites,j+num_sites,k+num_sites,l+num_sites] = 0.5*val  # convention with 0.5 factor included.
                eg_h2_1[i+num_sites,j+num_sites,k,l] = eg_h2_1[i,j,k+num_sites,l+num_sites] = 0.5*val

    print(numpy.where(eg_h2_1 > 0.01))
    quit()

    return eg_h1_0,eg_h1_1,eg_h2_0,eg_h2_1

def qubitOp(h1,h2):
    fer_op=fermionic_op_builder.build_ferm_op_from_ints(h1,h2)
    qubit_conv = QubitConverter(ParityMapper(), two_qubit_reduction=True, z2symmetry_reduction="auto")
    qubit_op = qubit_conv.convert(fer_op, len(h1)//2)
    #mapper=JordanWignerMapper()
    #qubit_op=mapper.map(fer_op)
    return qubit_op

def LRF_2_body_OD_terms(h2): #For 2 body off-diagonal terms
    def reshape_vec_to_mat(eigs):
        weight,vec1,vec2=eigs
        num_qubits=int(numpy.sqrt(vec1.shape[0]))
        L1=numpy.reshape(vec1,(num_qubits,num_qubits))
        L2=numpy.reshape(vec2,(num_qubits,num_qubits))

        print(numpy.max(abs(L1-L2.T)), 'check1')
        quit()


        return [numpy.sqrt(weight)*L1,numpy.sqrt(weight)*L2]
    #checksum
    def higherRankRep(L):
        T=numpy.zeros((num_qubits,num_qubits,num_qubits,num_qubits))*1j
        for p in range(num_qubits):
            for q in range(num_qubits):
                for r in range(num_qubits):
                    for s in range(num_qubits):
                        T[p,q,r,s]=L[p,q]*L[r,s]

        return T
    #constructing Low rank factorization circuits
    #Super matrix rep of four rank tensor (NxNxNxN)->(N^2xN^2)
    num_qubits=len(h2)
    N2=num_qubits*num_qubits
    h2_pq_rs=numpy.reshape(h2,(N2,N2))
    #Diagonalize Super matrix
    u,d,v=numpy.linalg.svd(h2_pq_rs,full_matrices=True)


    print(numpy.max(abs(u[:,0] - v[0,:])))

    print(d)

    w, v = numpy.linalg.eigh(h2_pq_rs)
    print(w)

    #Build Cholesky vectors
    Larr=[]
    arr=[(d[i],u[:,i],v[i]) for i in range(len(v)) if d[i]>1e-4]
    Larr=list(map(reshape_vec_to_mat,arr))
    #print(Larr)
    Larr=[Larr[i][0] for i in range(len(Larr))]
    qubit_h2=qubitOp(numpy.zeros((8,8)),h2)
    num_qubits_red=qubit_h2.num_qubits
    U1=PauliSumOp.from_list([('I'*num_qubits_red,numpy.cos(numpy.pi/4)),('IIIIIY',-1j*numpy.sin(numpy.pi/4))])
    U2=PauliSumOp.from_list([('I'*num_qubits_red,numpy.cos(numpy.pi/4)),('IIYIII',-1j*numpy.sin(numpy.pi/4))])
    rotH=(U2@U1@qubit_h2@U1.adjoint()@U2.adjoint()).reduce()
    qc=QuantumCircuit(num_qubits_red)
    qc=add_multiqubit_gate('IIIIIY',numpy.pi/4,qc)
    qc=add_multiqubit_gate('IIYIII',numpy.pi/4,qc)
    return qc,rotH,qubit_h2

def add_multiqubit_gate(pauli_string, param, circuit):
    num_qubits=circuit.num_qubits
    qr=QuantumRegister(num_qubits,'q')
    if pauli_string == 'I'*num_qubits:
        gate = 1
        for j in range(len(pauli_string)):
            gate = numpy.kron(gate, Pauli('I').to_matrix())
        gate *= -1j * np.sin(param)
        gate += numpy.cos(param) * np.eye(2**num_qubits)
        circuit.unitary(gate, qr, label=pauli_string)
    else:
        qubits_to_act_on = []
        gate = 1
        for j in range(len(pauli_string)):
            if pauli_string[j] != 'I':
                gate=numpy.kron(Pauli(pauli_string[j]).to_matrix(),gate)
                qubits_to_act_on.append(num_qubits-j-1)
        gate *= (-1j * numpy.sin(param))
        gate += numpy.cos(param) * numpy.eye(2**len(qubits_to_act_on))
        List_regs=[qr[i] for i in qubits_to_act_on]#[::-1]
        Label=pauli_string+"\n{:0.02f}".format(param.real)
        circuit.unitary(gate, List_regs, label = Label)
    return circuit

def get_givens_rotns(h):
        num_qubits=len(h)
        qubitH=qubitOp(h,numpy.zeros((num_qubits,num_qubits,num_qubits,num_qubits)))
        num_qubits_red=qubitH.num_qubits
        H=qubitH.to_matrix()
        w,v=numpy.linalg.eigh(h)
        givens=givensRotns(v.T)[0]
        rotH=qubitH
        circ=QuantumCircuit(num_qubits_red)
        for rots in givens:
            for tup in rots:
                i,j,theta,phi=tup
                h1=numpy.zeros((num_qubits,num_qubits))*1j
                h1[i,j]=-1j
                h1[j,i]=1j
                qubit_h1=qubitOp(h1,numpy.zeros((num_qubits,num_qubits,num_qubits,num_qubits)))
                List=qubit_h1.primitive.to_list()
                Op=PauliSumOp.from_list([('I'*num_qubits_red,1)])
                for i in range(len(List)):
                    Op=PauliSumOp.from_list([('I'*num_qubits_red,numpy.cos(theta*List[i][1])),(List[i][0],1j*numpy.sin(theta*List[i][1]))])@Op
                    circ=add_multiqubit_gate(List[i][0], theta*List[i][1], circ)
                rotH=Op@rotH@Op.adjoint()
                rotH=rotH.reduce().reduce()
        return circ,rotH,qubitH

def doubly_decomposed_form(h1_D,h1_X,h2_D,h2_X):

    #getting the givens rotation circuit and the rotated Hamiltonian for the one body terms
    circ_1_body,rotH_1_X,qubitH_1_X=get_givens_rotns(h1_X)
    #getting Cholesky vectors for two body off-diag terms
    circ_2_body,rotH_2_X,qubitH_2_X=LRF_2_body_OD_terms(h2_X)
    qubitH_D=qubitOp(h1_D,h2_D)
    circs_and_rotH_arr=[(circ_1_body,rotH_1_X,qubitH_1_X),(circ_2_body,rotH_2_X,qubitH_2_X),(qubitH_D)]
    return circs_and_rotH_arr

def expectation(h,circ):
    state=execute(circ,Aer.get_backend('statevector_simulator'),shots=1024).result().get_statevector()
    h_expec=state.conj()@h.to_matrix()@state
    return h_expec


def hamiltonian_expectation_in_doubly_decomposed_form_sv(params):

    ansatz_circ=var_form_base.construct_circuit(params)
    circ_rot_Arr=[ansatz_circ.copy(),ansatz_circ.copy(),ansatz_circ.copy()]
    h_part_expectation_arr=[]
    circ_rot_Arr[0]=circ_rot_Arr[0]+circs_and_rotH[0][0]
    h_part_expectation_arr.append(expectation(circs_and_rotH[0][1],circ_rot_Arr[0]))
    circ_rot_Arr[1]=circ_rot_Arr[1]+circs_and_rotH[1][0]
    h_part_expectation_arr.append(expectation(circs_and_rotH[1][1],circ_rot_Arr[1]))
    h_part_expectation_arr.append(expectation(circs_and_rotH[2],circ_rot_Arr[2]))
    h_sum_DD=sum(h_part_expectation_arr).real
    qubitH=qubitOp(h1_D+h1_X,h2_X+h2_D)
    h_sum_direct=expectation(qubitH,ansatz_circ).real
    print("doubly decomposed",h_sum_DD)
    print("direct",h_sum_direct)
    return h_sum_DD

def hamiltonian_estimation_in_doubly_decomposed_form_qasm(params,num_shots_arr):

    def Energy_Estimator_DD(input_vars):

        def getCounts(Input):
            circ,num_shots=Input
            circ1=circ.copy()
            circ1.add_register(ClassicalRegister(6,'c'))
            circ1.measure([0,1,2,3,4,5],[0,1,2,3,4,5])
            job_sim = execute(circ1, AerSimulator(method='density_matrix'),shots=num_shots)
            result_sim = job_sim.result()
            #density_matrix = result_sim
            counts = result_sim.get_counts(circ1)
            return counts#,density_matrix
        def estimate(PauliOp,counts):
            PauliString,coeff=PauliOp.primitive.to_list()[0]
            indexOfZs=numpy.where(numpy.array(list(PauliString))=='Z')[0]
            bitstrings,cnts=list(counts. keys()),list(counts.values())
            p=0
            count_valid=0
            for i in range(len(bitstrings)):
                bit_Str_to_Arr=numpy.array(list(bitstrings[i]))
                #flag1=1 if len(numpy.where(bit_Str_to_Arr=='1')[0])==4 else 0 #check number of electrons
                #flag2=1 if len(numpy.where(bit_Str_to_Arr[0:4]=='1')[0])==2 else 0 #check net spin
                #if ((flag1==1) and (flag2==1)):
                #count_valid=count_valid+cnts[i]
                counter_1=list(numpy.array(list(bitstrings[i]))[indexOfZs]).count('1')
                if counter_1%2==1:
                    p=p+cnts[i]
            p=p/sum(cnts)
            mean=(1-2*p)
            return mean*coeff.real
        def meanOp(PauliOp,counts):
            mean=0
            for i in range(len(PauliOp)):
                mean=mean+estimate(PauliOp[i],counts)
            return mean
        def covariance(Op,i,j,counts):
            cov=estimate(Op[i]@Op[j],counts)-estimate(Op[i],counts)*estimate(Op[j],counts)
            return cov
        def covbtnOps(A,B,counts):
            corr=0
            for i in range(len(A)):
                for j in range(len(B)):
                    corr=corr+estimate(A[i]@B[j],counts)-estimate(A[i],counts)*estimate(B[j],counts)
            return corr
        def variance(Op,counts):
            var=0
            cov_mat=numpy.zeros((len(Op),len(Op)))
            for i in range(len(Op)):
                for j in range(i,len(Op)):
                    if i==j:
                        mean=estimate(Op[i],counts)
                        coeff=Op[i].primitive.to_list()[0][1].real
                        cov_mat[i][j]=(coeff**2-mean**2)
                        var=var+(coeff**2-mean**2)
                    elif i!=j:
                        var=var+2*covariance(Op,i,j,counts)
                        cov_mat[i][j]=cov_mat[j][i]=covariance(Op,i,j,counts)
            return var
        def OpErr(data):
            Op,counts=data
            var=variance(Op,counts)
            err=numpy.sqrt(var/sum(counts.values()))
            return err.real
        circ,Op,num_shots=input_vars
        counts_Arr=getCounts((circ,num_shots))
        m1=meanOp(Op,counts_Arr)
        err1=OpErr((Op,counts_Arr))
        return m1,err1
    #params,num_shots_arr=input_vars
    ansatz_circ=var_form_base.construct_circuit(params)
    circ_rot_Arr=[ansatz_circ.copy(),ansatz_circ.copy(),ansatz_circ.copy()]
    h_part_expectation_arr=[]
    circ_rot_Arr[0]=circ_rot_Arr[0]+circs_and_rotH[0][0]
    circ_rot_Arr[1]=circ_rot_Arr[1]+circs_and_rotH[1][0]
    #Estimate from statistics
    data_arr=[(circ_rot_Arr[0],circs_and_rotH[0][1],num_shots_arr[0]),
              (circ_rot_Arr[1],circs_and_rotH[1][1],num_shots_arr[1]),
              (circ_rot_Arr[2],circs_and_rotH[2],num_shots_arr[2])]
    results=Parallel(n_jobs=1,verbose=2)(delayed(Energy_Estimator_DD)(data_arr[i]) for i in range(len(data_arr)))
    m1,err1=results[0][0],results[0][1]
    m2,err2=results[1][0],results[1][1]
    m3,err3=results[2][0],results[2][1]
    if print_res==True:
        print("total mean",m1+m2+m3,"total error",err1+err2+err3,"ind means",m1,m2,m3,"ind errs",err1,err2,err3)
    save_opt_steps=True
    if save_opt_steps==True:
        with open('OptStepsWithQasm_parity.txt','a') as f:
            Str=["{:0.16f}".format(params[i]) for i in range(len(params))]
            print('['+','.join(Str)+']'+'#'+"{:0.16f}".format(Energy(params)),file=f)
    return m1+m2+m3#,err1+err2+err3,m1,m2,m3,err1,err2,err3
def Energy(params,save_opt_steps=False):
    circ=var_form_base.construct_circuit(parameters=params)
    state=execute(circ,Aer.get_backend('statevector_simulator'),shots=1024).result().get_statevector()
    E=(state.conj()@Hmat@state).real
    if save_opt_steps==True:
        with open('OptStepsWithQasm_parity.txt','a') as f:
            Str=["{:0.16f}".format(params[i]) for i in range(len(params))]
            #Str2=["{:0.16f}".format(elem) for elem in [E,MSE,Energy(params)]]
            print('['+','.join(Str)+']'+'#'+"{:0.16f}".format(Energy(params)),file=f)
    return E


# In[175]:


def commutator(A,B):
        B2=WeightedPauliOperator([[-B.paulis[0][0],B.paulis[0][1]]])
        return A.multiply(B).add(B2.multiply(A))
def commutatorsForGradient(Op):
    Op_conj=WeightedPauliOperator([[-1j,Op.paulis[0][1]]])
    M=Op.multiply(qubitH).add(qubitH.multiply(Op_conj))
    Mmat=op_converter.to_matrix_operator(M).matrix
    return Mmat
def commutatorPool(qubitH,stripZs=False):
    def stripZ(label):
        label=numpy.array(list(label))
        label[label=='Z']='I'
        label=''.join(list(label))
        return label
    #construct commutator pool from the Hamiltonian
    pool_H=[WeightedPauliOperator([[1j,qubitH.paulis[i][1]]]) for i in range(len(qubitH.paulis))][1:]
    #commutator between operators
    commutator_pool=[WeightedPauliOperator([[1j,commutator(op1,op2).paulis[0][1]]]) for op1 in pool_H for op2 in pool_H if countYgates(commutator(op1,op2).paulis[0][1].to_label())%2==1]
    labels=[commutator_pool[i].paulis[0][1].to_label() for i in range(len(commutator_pool))]
    unique_labels=numpy.unique(labels)
    if stripZs==True:
        new_labels=[]
        for i in range(len(unique_labels)):
            new_labels.append(stripZ(unique_labels[i]))
        unique_labels=numpy.unique(new_labels)
    commutator_pool=[WeightedPauliOperator([[1j,Pauli(unique_labels[i])]]) for i in range(len(unique_labels))]
    return commutator_pool
# def compute_gradient(M,state):
#     grad=state@M@numpy.conjugate(state)
#     return grad.real
def compute_gradient(op,params):
    num_shots_arr=[2**18,2**18,2**18]
    var_form_base.push_hopping_operator(op)
    E1=hamiltonian_estimation_in_doubly_decomposed_form_qasm(params+[numpy.pi/4.],num_shots_arr)
    E2=hamiltonian_estimation_in_doubly_decomposed_form_qasm(params+[-numpy.pi/4.],num_shots_arr)
    gradient=(E1-E2)/2
    var_form_base.pop_hopping_operator()
    return gradient


# In[176]:



#SMO with qasm
def SMO_qasm(cost,params,runs=20,tol=1e-4,save_opt_steps=False):
    index=1
    conv_err=1000
    t1=time.time()
    def Energy(params):
        backend = Aer.get_backend('statevector_simulator')
        circ=var_form_base.construct_circuit(parameters=params)
        stateVector_0=execute(circ,backend,shots=1024).result().get_statevector()
        E=numpy.real(numpy.dot(numpy.dot(numpy.conjugate(stateVector_0),Hmat),stateVector_0))
        return E
    def E_landscape(ind,ang,cost,params):
        params1=copy.deepcopy(params)
        params1[ind]=params1[ind]+ang #ang
        data=cost(params1,num_shots_arr)
        return data
    def determine_unknowns(E,cost,params,ind):
        L1=E
        L2=E_landscape(ind,numpy.pi/4.,cost,params)
        L3=E_landscape(ind,-numpy.pi/4.,cost,params)
        ratio=(L3-L2)/(2*L1-L2-L3)
        a3=(L2+L3)/2.
        a2=2*params[ind]-numpy.arctan(ratio)
        a1=(L1-a3)/numpy.cos(numpy.arctan(ratio))
        return a1,a2,a3
    def update(E,cost,params,ind):
        t1=time.time()
        a1,a2,a3=determine_unknowns(E,cost,params,ind)
        thetaStar=a2/2.+numpy.pi/2. if a1>0 else a2/2.
        newParams=copy.deepcopy(params)
        newParams[ind]=thetaStar
        updEnergy=cost(newParams,num_shots_arr)
        #print("time taken",time.time()-t1)
        return newParams,updEnergy
    while conv_err>tol and index<runs:
        print("looped "+str(index)+" times")
        if(index==1):
            E=cost(params,num_shots_arr)
            params,E=update(E,cost,params,-1)
            Eold=E
        else:
            Eold=cost(params,num_shots_arr)
        init=0
        for i in range(len(params)):#[::-1][0:1]:
            #first run sequential minimial optimization (SMO)  for a given multiqubit operator using
            #exact analytical form for cost function
            if init==0:
                E=Eold
            ind=i
            params,E=update(E,cost,params,ind)
            print("upd_energy",Energy(params))
            if save_opt_steps==True:
                with open('OptStepsWithQasm_parity.txt','a') as f:
                    Str=["{:0.16f}".format(params[i]) for i in range(len(params))]
                    #Str2=["{:0.16f}".format(elem) for elem in [E,MSE,Energy(params)]]
                    print('['+','.join(Str)+']'+'#'+"{:0.16f}".format(Energy(params)),file=f)
            else:
                continue
            init=init+1
        conv_err=numpy.abs(Eold-E)
        print("inner loop error",conv_err)
        index=index+1
    res={'x':params,'fun':E}
    return res
#Adadelta with Qasm
def AdaDelta_qasm(cost,params,runs=20,tol=1e-4,num_shots_arr=[2**8,2**9,2**9],save_opt_steps=False):
    string_shots_label=','.join([str(shots) for shots in num_shots_arr])
    v=numpy.zeros((len(params)))*0.0
    delta=numpy.zeros((len(params)))*0.0
    beta=0.9
    rho=0.8
    directions= numpy.arange(len(params))
    delta=0
    conv_err=1
    old_conv_err=1
    Eold=cost(params,num_shots_arr)
    E_arr=[]
    ind=0
    eps=numpy.array([5e-8]*len(params))#1e-5,5e-6,1e-7,5e-8
    Max=1e5
    lr0=0.05

    def learningRateEnv(lr0,runs):
        lr_u=lr0+Max*numpy.exp(-(28/runs)*numpy.linspace(0,runs,runs+1))
        lr_d=lr0*(1-numpy.exp(-(28/runs)*numpy.linspace(0,runs,runs+1)))
        return lr_u,lr_d
    def Energy(params):
        backend = Aer.get_backend('statevector_simulator')
        circ=var_form_base.construct_circuit(parameters=params)
        stateVector_0=execute(circ,backend,shots=1024).result().get_statevector()
        E=numpy.real(numpy.dot(numpy.dot(numpy.conjugate(stateVector_0),Hmat),stateVector_0))
        return E
    def StochGrad(cost,params,directions,num_shots_arr):
        def Energy_Estimator_DD(input_vars):

            def getCounts(Input):
                circ,num_shots=Input
                circ1=circ.copy()
                circ1.add_register(ClassicalRegister(6,'c'))
                circ1.measure([0,1,2,3,4,5],[0,1,2,3,4,5])
                job_sim = execute(circ1, AerSimulator(method='density_matrix'),shots=num_shots)
                result_sim = job_sim.result()
                #density_matrix = result_sim
                counts = result_sim.get_counts(circ1)
                return counts#,density_matrix
            def estimate(PauliOp,counts):
                PauliString,coeff=PauliOp.primitive.to_list()[0]
                indexOfZs=numpy.where(numpy.array(list(PauliString))=='Z')[0]
                bitstrings,cnts=list(counts. keys()),list(counts.values())
                p=0
                count_valid=0
                for i in range(len(bitstrings)):
                    bit_Str_to_Arr=numpy.array(list(bitstrings[i]))
                    counter_1=list(numpy.array(list(bitstrings[i]))[indexOfZs]).count('1')
                    if counter_1%2==1:
                        p=p+cnts[i]
                p=p/sum(cnts)
                mean=(1-2*p)
                return mean*coeff.real
            def meanOp(PauliOp,counts):
                mean=0
                for i in range(len(PauliOp)):
                    mean=mean+estimate(PauliOp[i],counts)
                return mean
            def covariance(Op,i,j,counts):
                cov=estimate(Op[i]@Op[j],counts)-estimate(Op[i],counts)*estimate(Op[j],counts)
                return cov
            def covbtnOps(A,B,counts):
                corr=0
                for i in range(len(A)):
                    for j in range(len(B)):
                        corr=corr+estimate(A[i]@B[j],counts)-estimate(A[i],counts)*estimate(B[j],counts)
                return corr
            def variance(Op,counts):
                var=0
                cov_mat=numpy.zeros((len(Op),len(Op)))
                for i in range(len(Op)):
                    for j in range(i,len(Op)):
                        if i==j:
                            mean=estimate(Op[i],counts)
                            coeff=Op[i].primitive.to_list()[0][1].real
                            cov_mat[i][j]=(coeff**2-mean**2)
                            var=var+(coeff**2-mean**2)
                        elif i!=j:
                            var=var+2*covariance(Op,i,j,counts)
                            cov_mat[i][j]=cov_mat[j][i]=covariance(Op,i,j,counts)
                return var
            def OpErr(data):
                Op,counts=data
                var=variance(Op,counts)
                err=numpy.sqrt(var/sum(counts.values()))
                return err.real
            circ,Op,num_shots=input_vars
            counts_Arr=getCounts((circ,num_shots))
            m1=meanOp(Op,counts_Arr)
            err1=OpErr((Op,counts_Arr))
            return m1,err1
        shiftedParams1=[numpy.array(params)+numpy.array([0]*ei+[numpy.pi/4.]+[0]*(len(params)-ei-1)) for ei in directions]
        shiftedParams2=[numpy.array(params)+numpy.array([0]*ei+[-numpy.pi/4.]+[0]*(len(params)-ei-1)) for ei in directions]
        stochGrads=numpy.zeros((len(params)))
        data_arr=[]
        for i in range(len(shiftedParams1)):
            circ=var_form_base.construct_circuit(parameters=shiftedParams1[i])
            circ_arr=[circ.copy(),circ.copy(),circ.copy()]
            circ_arr[0]=circ_arr[0]+circs_and_rotH[0][0]
            circ_arr[1]=circ_arr[1]+circs_and_rotH[1][0]
            data_arr=data_arr+[(circ_arr[0],circs_and_rotH[0][1],num_shots_arr[0]),
                      (circ_arr[1],circs_and_rotH[1][1],num_shots_arr[1]),
                      (circ_arr[2],circs_and_rotH[2],num_shots_arr[2])]
        for i in range(len(shiftedParams2)):
            circ=var_form_base.construct_circuit(parameters=shiftedParams2[i])
            circ_arr=[circ.copy(),circ.copy(),circ.copy()]
            circ_arr[0]=circ_arr[0]+circs_and_rotH[0][0]
            circ_arr[1]=circ_arr[1]+circs_and_rotH[1][0]
            data_arr=data_arr+[(circ_arr[0],circs_and_rotH[0][1],num_shots_arr[0]),
                      (circ_arr[1],circs_and_rotH[1][1],num_shots_arr[1]),
                      (circ_arr[2],circs_and_rotH[2],num_shots_arr[2])]
        results=Parallel(n_jobs=1,verbose=2)(delayed(Energy_Estimator_DD)(data_arr[i]) for i in range(len(data_arr)))
        estm1Arr=[results[i][0]+results[i+1][0]+results[i+2][0] for i in range(0,len(results)//2,3)]#map(cost,shiftedParams1)
        estm2Arr=[results[i][0]+results[i+1][0]+results[i+2][0] for i in range(len(results)//2,len(results),3)]
        stochGrads=0.5*(numpy.array(list(estm1Arr))-numpy.array(list(estm2Arr)))
        return stochGrads
    lr_u,lr_d= learningRateEnv(lr0,runs)
    flag=0
    flag1=0
    terminate=False
    while ((conv_err>tol) and (ind<runs)):
        print("index",ind)
        old_conv_err=conv_err
        g_stoch=StochGrad(cost,params,directions,num_shots_arr)
        v = beta*v+(1-beta)*g_stoch*g_stoch
        lr=numpy.sqrt(delta+eps)/numpy.sqrt(v+eps)  #learning rate
        #clipping learning rates, increasing shots and restarting ada delta
        if ((conv_err<1e-4) and (flag==0)):
            flag=1
            num_shots_arr=[2**19,2**18,2**19]
            print("************shots increased**************")
        if ((conv_err<5e-7) and (flag1==0)):
            flag1=1
            num_shots_arr=[2**21,2**20,2**21]
            print("************shots increased**************")
        for i in range(len(lr)):
            if lr[i]>lr_u[ind]:
                eps[i]=lr_u[ind]*lr_u[ind]*v[i]
                lr[i]=lr_u[ind]
                delta[i]=0
            elif lr[i]<lr_d[ind]:
                eps[i]=lr_d[ind]*lr_d[ind]*v[i]
                lr[i]=lr_d[ind]
                delta[i]=0
            else:
                continue
        g=lr*g_stoch
        params=params-g
        params=params.real
        delta= beta*delta+(1-beta)*g*g
        E=Energy(params)#HamiltonianEstm(params)[0]
        E_arr.append(E)
        print("updated energy",E,"learning rate",lr)#,Energy(params))
        if save_opt_steps==True:
            with open('OptStepsWithQasm_parity.txt','a') as f:
                print('['+','.join([str(params[i]) for i in range(len(params))])+']'+'#'+str(E),file=f)
        ind=ind+1
        conv_err=rho*conv_err+(1-rho)*numpy.abs(Eold-E)
        print("error",conv_err)
        Eold=E
    res={'x':params,'fun':E}
    return res


# In[177]:


U=7
h1_D,h1_X,h2_D,h2_X=egBandHamiltonianPartition(U)
circs_and_rotH=doubly_decomposed_form(h1_D,h1_X,h2_D,h2_X)


# In[178]:


#preparing commutator pool
def qubitWeightedOp(h1,h2):
    qubit_op=FermionicOperator(h1,h2).mapping('parity')
    qubitH=Z2Symmetries.two_qubit_reduction(qubit_op, qubit_op.num_qubits//2)
    return qubitH
qubitH=qubitWeightedOp(h1_D+h1_X,h2_D+h2_X)
Hmat=op_converter.to_matrix_operator(qubitH).dense_matrix
w,v=numpy.linalg.eigh(Hmat)
Eg=w[0]
commutator_pool=commutatorPool(qubitH,stripZs=True)
#constructing matrices for gradient computation
MatrixRepOfPoolOps=list(map(commutatorsForGradient,commutator_pool))
print("pool sizes",len(MatrixRepOfPoolOps))
backend=Aer.get_backend('statevector_simulator')
#preparing init s>tate
circ=QuantumCircuit(6)
circ.x(0)
circ.x(3)
var_form_base=UCCSD(8,num_particles=4, initial_state=circ,qubit_mapping='parity',two_qubit_reduction=True)
var_form_base.manage_hopping_operators()
error=1000


# In[155]:


#import time


# In[156]:


# with open('OptStepsWithQasm_parity.txt','r') as f:
#     lines=f.readlines()
#     Labels=[]
#     EnergyArr=[]
#     for i in range(len(lines)):
#         if lines[i][0]=='l':
#             Labels.append(lines[i].split('-')[1][1:-1])
#             if i!=0:

#                 EnergyArr.append(eval(lines[i-1].split('#')[1]))
#     EnergyArr.append(eval(lines[-1].split('#')[1]))
#     params=eval(lines[-1])
# print(Labels)
# for i in range(len(Labels)):
#     var_form_base.push_hopping_operator(WeightedPauliOperator([[1j,Pauli(Labels[i])]]))


# In[142]:


# print_res=True
# hamiltonian_estimation_in_doubly_decomposed_form_qasm(params,[2**22,2**21,2**22])


# In[143]:


#ExcOps=Labels


# In[144]:


#EnergyArr


# In[145]:


#len(params),var_form_base.num_parameters


# In[146]:


#res=SMO_sv(Energy,params,runs=40,tol=1e-6,save_opt_steps=True)


# In[147]:


#num_shots_arr=[2**22,2**21,2**22]
#save_opt_steps=False
#print_res=True
#res=SMO_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=40,tol=1e-6,save_opt_steps=True)
#res=scipy.optimize.minimize(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,args=([2**22,2**22,2**23]),method='BFGS',options={'ftol':1e-6})
#res=scipy.optimize.minimize(Energy,params,args=(True),method='BFGS',options={'ftol':1e-6})


# In[148]:


# print_res=False
# res=AdaDelta_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=400,tol=1e-7,num_shots_arr=[2**18,2**17,2**18],save_opt_steps=True)


# In[149]:


#res=AdaDelta_sv(Energy,params,runs=400,tol=1e-7,save_opt_steps=True)


# In[172]:


params=[]
EnergyArr=[]
ExcOps=[]
ti=time.time()
steps=35
num_shots_arr=[2**22,2**21,2**22] # we can change this to [2**18,2**18,2**18] it still works
print_res=False
for i in range(35):
    #save_steps=False
    #circ=var_form_base.construct_circuit(parameters=params)
    #state=execute(circ,backend,shots=1024).result().get_statevector()
    #grads=numpy.array(Parallel(n_jobs=7,verbose=2)(delayed(compute_gradient)(MatrixRepOfPoolOps[i],state) for i in range(len(commutator_pool))))#qubit_pool_UCCSD))))#
    grads=numpy.array(Parallel(n_jobs=1,verbose=2)(delayed(compute_gradient)(op,params) for op in commutator_pool))
    indexes=numpy.argsort(abs(grads))[::-1][:5]
    #Labels=[commutator_pool[indexes[i]].paulis[0][1].to_label() for i in range(len(indexes))]
    print("5 highest grads",grads[indexes[0]],grads[indexes[1]],grads[indexes[2]],grads[indexes[3]],grads[indexes[4]])
    if len(ExcOps)!=0:
        #overlap_scores=[overlap_score(ExcOps[-1],Labels[i]) for i in range(len(Labels))]
        #if (min(overlap_scores)==0):
        #    indexes=indexes[numpy.argsort(overlap_scores)]
        #else:
        #    indexes=indexes
        if (commutator_pool[indexes[0]].paulis[0][1].to_label()!=ExcOps[-1]):
            PauliOp=commutator_pool[indexes[0]]
        else:
            PauliOp=commutator_pool[indexes[1]]
    else:
        PauliOp=commutator_pool[indexes[0]]
    ExcOps.append(PauliOp.paulis[0][1].to_label())
    print("chosen Op",ExcOps[-1])
    with open('OptStepsWithQasm_parity.txt','a') as f:
        print("label-",ExcOps[-1],file=f)
    params.append(0.0)
    var_form_base.push_hopping_operator(PauliOp)
    res=SMO_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=7,tol=1e-4,save_opt_steps=True)
    params,E=list(res['x']),res['fun']
    res=AdaDelta_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=200,tol=1e-6,num_shots_arr=[2**18,2**18,2**18],save_opt_steps=True)#SMO_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=40,tol=1e-6,save_opt_steps=True)    #res=scipy.optimize.minimize(Energy_Estimator,params,method='BFGS',options={'ftol':1e-7})
    params,E=list(res['x']),res['fun']
    print("num_params",var_form_base.num_parameters)
    print("Energy",E)
    EnergyArr.append(E)
    print("time elapsed",time.time()-ti)
    error=EnergyArr[-1]-Eg


# In[167]:


indexes=numpy.argsort(abs(grads))[::-1][:5]
label=[commutator_pool[i].paulis[0][1].to_label() for i in range(len(indexes))]


# In[168]:


label


# In[169]:


grads


# In[ ]:




