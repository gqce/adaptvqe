#!/usr/bin/env python
from model import model
from ansatz import ansatz
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-m", "--hmode", type=int, default=0,
        help="Hamiltonian mode. 0: mo representation; 1: ao rep. dflt: 0.")
parser.add_argument("--noopspm", action="store_true",
        help="use native qutip. not recommended. for debug only.")
parser.add_argument("--includepoolz", action="store_true",
        help="use pool with zs.")
parser.add_argument("--mingeneratortypes", action="store_true",
        help="minimize generator types.")
parser.add_argument("-r", "--refmode", type=int, default=0,
        help="reference state. 0: hf product state; 1: givens;" + \
        " else: gs of h1ao. dflt: 0.")
parser.add_argument("-p", "--pmode", type=int, default=0,
        help="pool choice. 0: sUCCSpD; 1: H commutator pool. dflt: 0.")
parser.add_argument("-w", "--wpenalty", type=float, default=10,
        help="penalty weight for N. dflt: 10. bigger value for jw" +\
        " than parity due to more degrees of freedom.")
parser.add_argument("-t", "--tol", type=float, default=0.001,
        help="gradient cut-off.")
parser.add_argument("-o", "--optmode", type=int, default=0,
        help="optimizer choice. 0: BFGS; else: seq_batch_opt.  dflt: 0.")
parser.add_argument("-a", "--maxadd", type=int, default=1,
        help="maximal number of ops to be added each iteration. dflt: 1.")

args = parser.parse_args()
opspm = not args.noopspm

mdl = model(
        hmode=args.hmode,
        wpenalty=args.wpenalty,
        )
ans = ansatz(mdl,
        opspm=opspm,
        refmode=args.refmode,
        pmode=args.pmode,
        maxadd=args.maxadd,
        optmode=args.optmode,
        filterz=not args.includepoolz,
        tol=args.tol,
        mingenerators=args.mingeneratortypes,
        )
# save initail state
state_i = ans.get_state()
# run optimization
ans.additive_optimize()
# post-analysis
w, v = mdl.get_loweste_states()
print(f'reference state energy: {mdl.get_h_expval(ans._ref_state):.8f}')
print(f'initial state energy: {mdl.get_h_expval(state_i):.8f}')
print("lowest energies: ", w)
print(f"cost: {ans._cost:.8f}")

if opspm:
    fidelity_f = abs(ans.get_state().dot(v[0].conj()))**2
    fidelity_i = abs(state_i.dot(v[0].conj()))**2
else:
    fidelity_f = abs(ans.get_state().overlap(v[0]))**2
    fidelity_i = abs(state_i.overlap(v[0]))**2
print("fidelity initial: ", fidelity_i, " final: ", fidelity_f, flush=True)
print(f"final generators: {ans._ansatz[1]}")
print(f"len of unique generators: {len(set(ans._ansatz[1]))}")
