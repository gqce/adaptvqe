#!/usr/bin/env python
# coding: utf-8

# In[173]:


from qiskit.opflow import StateFn, AbelianGrouper
from qiskit import QuantumCircuit, execute, QuantumRegister, ClassicalRegister
import warnings
from joblib import Parallel, delayed
from qiskit_nature.mappers.second_quantization import JordanWignerMapper, ParityMapper
import scipy
from qiskit.circuit.random import random_circuit
from qiskit import Aer
from openfermion.circuits import slater_determinant_preparation_circuit
from qiskit.quantum_info import Pauli
from qiskit_nature.problems.second_quantization.electronic.builders import fermionic_op_builder
from qiskit.opflow.primitive_ops import PauliSumOp
from openfermion.linalg import givens_decomposition_square as givensRotns
from qiskit.providers.aer import AerSimulator
from qiskit_nature.converters.second_quantization import QubitConverter
import qiskit
import copy
import os, numpy, time, h5py, itertools

from qiskit_nature.operators.second_quantization.fermionic_op import  FermionicOp
from qiskit.circuit import Parameter

from timing import timeit



def warn(*args, **kwargs):
    pass

warnings.warn = warn


def exp_all_z(
        circuit,
        pauli_ids,
        t,
        quantum_register=None,
        control_qubit=None,
        ):
    """
    origin: https://github.com/DavitKhach/quantum-algorithms-tutorials
    The implementation of exp(-iZZ..Z t), where Z is
    the Pauli Z operator, t is a parameter.
    :param circuit: QuantumCircuit.
    :param quantum_register: QuantumRegister.
    :param pauli_ids: the indexes from quantum_register that
                         correspond to entries not equal to I:
                         e.g. if we have XIYZI then the
                         pauli_ids = [0,2,3].
    :param control_qubit: the control Qubit from QuantumRegister
                          other than quantum_register.
    :param t: the parameter t in exp(iZZ..Z t).
    """
    # the controlled_exp(iIt) special case
    if len(pauli_ids) == 0 and control_qubit is not None:
        circuit.add_register(control_qubit.register)
        circuit.u1(-t, control_qubit)
        return

    if quantum_register is None:
        quantum_register = QuantumRegister(circuit.num_qubits, 'q')

    # the first CNOTs
    for i, j in zip(pauli_ids, pauli_ids[1:]):
        circuit.cx(quantum_register[i], quantum_register[j])

    # Rz gate
    if control_qubit is None:
        circuit.rz(2*t, quantum_register[pauli_ids[-1]])
    else:
        circuit.add_register(control_qubit.register)
        circuit.crz(2*t,
                control_qubit,
                quantum_register[pauli_ids[-1]]
                )

    # the second CNOTs
    for i, j in zip(pauli_ids[-2::-1], pauli_ids[::-1]):
        circuit.cx(quantum_register[i], quantum_register[j])


def exp_pauli(pauli,
        t,
        quantum_register=None,
        control_qubit=None,
        ):
    """
    The circuit for the exp(-i P t), where P is the Pauli term,
    t is the parameter.
    :param pauli: the string for the Pauli term: e.g. "XIXY".
    :param quantum_register: QuantumRegister.
    :param control_qubit: the control Qubit from QuantumRegister
                          other than quantum_register.
    :param t: the parameter t in exp(i P t).
    :return: QuantumCircuit that implements exp(i P t) or
             control version of it.
    """
    if quantum_register is None:
        quantum_register = QuantumRegister(len(pauli), 'q')
    elif len(pauli) != len(quantum_register):
        raise Exception("Pauli string doesn't match to the quantum register")

    pauli_circuit = QuantumCircuit(quantum_register)
    circuit_bracket = QuantumCircuit(quantum_register)
    pauli_ids = []

    # qiskit cpnvention!
    for i, p in enumerate(pauli[::-1]):
        if p == 'I':
            continue
        elif p == 'Z':
            pauli_ids.append(i)
        elif p == 'X':
            circuit_bracket.h(quantum_register[i])
            pauli_ids.append(i)
        elif p == 'Y':
            circuit_bracket.u(numpy.pi/2, numpy.pi/2, numpy.pi/2, quantum_register[i])
            pauli_ids.append(i)

    pauli_circuit += circuit_bracket
    exp_all_z(pauli_circuit,
            pauli_ids,
            t,
            quantum_register=quantum_register,
            control_qubit=control_qubit,
            )
    pauli_circuit += circuit_bracket

    return pauli_circuit


class paramCircuits:
    def __init__(self,
            nq,              # register length
            paulis=[],       # list of Pauli generator labels
            ref_state=None,  # inital state label, e.g., '001001'
            ref_circ=None,   # circuit for reference state
            ):
        self._nq = nq
        self._paulis = []
        self._thetas = []
        self.init_circ(ref_state, ref_circ, paulis)

    def init_circ(self, ref_state, ref_circ, paulis):
        self._circ = QuantumCircuit(self._nq)
        # temparary circuit to be appended
        self._tmpcirc = None
        # reference state
        if ref_state is not None:
            for i, s in enumerate(ref_state[::-1]):
                if s == '1':
                    self._circ.x(i)
        if ref_circ is not None:
            self._circ += ref_circ
        # intial generators
        for i, pauli in enumerate(paulis):
            self.expand(i, pauli)

    def expand(self, pauli):
        self._paulis.append(pauli)
        itheta = len(self._paulis)
        theta = Parameter(str(itheta))
        self._circ += exp_pauli(pauli, theta)
        self._thetas.append(theta)

    def get_circuit(self, params):
        assert(len(params) == len(self._thetas))
        binds = {th: val for th, val in zip(self._thetas, params)}
        if self._tmpcirc is None:
            circ = self._circ.bind_parameters(binds)
        else:
            circ = (self._circ + self._tmpcirc).bind_parameters(binds)
        return circ

    def set_tmpcirc(self, pauli):
        if self._tmpcirc is None:
            theta = Parameter(str(len(self._paulis)+1))
            self._thetas.append(theta)
        else:
            theta = self._thetas[-1]
        self._tmpcirc = exp_pauli(pauli, theta)

        print(pauli, len(self._thetas))

    def del_tmpcirc(self):
        if self._tmpcirc is not None:
            self._thetas.pop()
            self._tmpcirc = None


def egBandHamiltonianPartition(
        imp=0,
        ):
    with h5py.File('HEmbed.h5', 'r') as f:
        # take one spin block
        h1e = f[f'/impurity_{imp}/H1E'][::2, ::2].T
        d = f[f'/impurity_{imp}/D'][::2, ::2].T
        lam = f[f'/impurity_{imp}/LAMBDA'][::2, ::2].T
        v2e = f[f'/impurity_{imp}/V2E'][::2, ::2, ::2, ::2].T
    # one spin block
    n1 = h1e.shape[0]
    n2 = n1*2
    h1emb = numpy.zeros((n2, n2))
    h1emb[:n1, :n1] = h1e
    h1emb[:n1, n1:] = d.T
    h1emb[n1:, :n1] = d
    h1emb[n1:, n1:] = -lam

    # off-diagonal components
    h1_1 = numpy.zeros((2*n2, 2*n2))
    h1_1[:n2, :n2] = h1_1[n2:, n2:] = h1emb

    # density-density components
    h2_0 = numpy.zeros((2*n2, 2*n2, 2*n2, 2*n2))
    h2_1 = numpy.zeros((2*n2, 2*n2, 2*n2, 2*n2))
    for i, j, k, l in itertools.product(range(n1), repeat=4):
        if i == j and k == l:
            h2_0[i, j, k, l] = h2_0[i+n2, j+n2, k, l] = \
                h2_0[i, j, k+n2, l+n2] = \
                h2_0[i+n2, j+n2, k+n2, l+n2] = 0.5*v2e[i, j, k, l]
        else:
            h2_1[i, j, k, l] = h2_1[i+n2, j+n2, k+n2, l+n2] = \
                0.5*v2e[i, j, k, l]
            h2_1[i+n2, j+n2, k, l] = h2_1[i, j, k+n2, l+n2] = \
                0.5*v2e[i, j, k, l]

    # c+c+cc --> c+c c+c will introduce a shift in h1
    h2 = h2_0 + h2_1
    h1_1 -= numpy.einsum('ikkj->ij', h2)   # factor 1/2 already taken care of.
    # diagonal components
    h1_0 = numpy.diag(numpy.diag(h1_1))
    h1_1 -= h1_0

    return h1_0, h1_1, h2_0, h2_1


def get_fermion_nnop(
        h1=None,
        h2=None,
        nq=None,
        tol=1e-8,
        ):
    # get fermion operator in the density form.
    if nq is None:
        if h1 is not None:
            nq = len(h1)
        elif h2 is not None:
            nq = len(h2)
        else:
            raise ValueError('nq cannot be determined.')
    fop = 0
    if h1 is not None:
        inds = numpy.where(abs(h1) > tol)
        for i, j in zip(*inds):
            fop += FermionicOp((f'+_{i} -_{j}', h1[i, j]),
                    register_length=nq)
    if h2 is not None:
        inds = numpy.where(abs(h2) > tol)
        for i, j, k, l in zip(*inds):
            fop += FermionicOp((f'+_{i} -_{j} +_{k} -_{l}', h2[i, j, k, l]),
                    register_length=nq)
    return fop


def qubitOp(
        h1=None,
        h2=None,
        fop=None,
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):
    if fop is None:
        fop = get_fermion_nnop(h1=h1, h2=h2)
    if fop != 0:
        qubit_conv = QubitConverter(
                mapper,
                two_qubit_reduction=two_qubit_reduction,
                z2symmetry_reduction=z2symmetry_reduction,
                )
        qubit_op = qubit_conv.convert(fop,
                num_particles=fop.register_length//2,
                )
    else:
        qubit_op = None
    return qubit_op


@timeit
def LRF_2_body_OD_terms(h2):
    # For 2 body off-diagonal terms
    # constructing Low rank factorization circuits
    # Super matrix rep of four rank tensor (NxNxNxN)->(N^2xN^2)
    num_qubits = len(h2)
    N2 = num_qubits*num_qubits
    h2_pq_rs = numpy.reshape(h2, (N2, N2))
    # Diagonalize Super matrix
    w, v = numpy.linalg.eigh(h2_pq_rs)

    # Build Cholesky vectors
    Larr = [(v[:, i]*numpy.sqrt(wi)).reshape(num_qubits, num_qubits)
            for i, wi in enumerate(w) if wi > 1.e-6]
    # consistent check
    assert(numpy.allclose(h2, numpy.einsum('pij,pkl->ijkl', Larr, Larr)))

    qubit_h2 = qubitOp(h2=h2)

    res = []
    for L1 in Larr:
        circuit, rotqh2 = get_givens_rotns(L1, qop=qubit_h2)
        res.append((circuit, rotqh2))

    return res


def add_multiqubit_gate(pauli_string, param, circuit):
    num_qubits = len(pauli_string)
    qr = QuantumRegister(num_qubits, 'q')
    if pauli_string == 'I'*num_qubits:
        gate = 1
        for j in range(len(pauli_string)):
            gate = numpy.kron(gate, Pauli('I').to_matrix())
        gate *= -1j * numpy.sin(param)
        gate += numpy.cos(param) * numpy.eye(2**num_qubits)
        circuit.unitary(gate, qr, label=pauli_string)
    else:
        qubits_to_act_on = []
        gate = 1
        for j in range(len(pauli_string)):
            if pauli_string[j] != 'I':
                gate = numpy.kron(Pauli(pauli_string[j]).to_matrix(), gate)
                qubits_to_act_on.append(num_qubits-j-1)
        gate *= (-1j * numpy.sin(param))
        gate += numpy.cos(param) * numpy.eye(2**len(qubits_to_act_on))
        list_regs = [qr[i] for i in qubits_to_act_on]
        label = pauli_string+"\n{:0.02f}".format(param.real)
        circuit.unitary(gate, list_regs, label=label)
    return circuit


@timeit
def get_givens_rotns(
        h,          # basis rotation which determines the given rotation
        qop=None,   # qubit operator to be rotated.
        mapper=ParityMapper(),
        two_qubit_reduction=True,
        z2symmetry_reduction="auto",
        ):

    # get given roation
    w, v = numpy.linalg.eigh(h)

    print('givens-v', numpy.round(v, decimals=3))
    givens = givensRotns(v.T)[0]

    circ = None
    op = None
    nrots = len(givens)
    for ir, rots in enumerate(givens):
        for tup in rots:
            i, j, theta, phi = tup
            print('applying givens' +
                    f' {ir}/{nrots} {i}, {j}, {theta:.3f}, {phi:.3f}')
            h1 = numpy.zeros_like(h)*1j
            h1[i, j] = -1j
            h1[j, i] = 1j
            qubit_h1 = qubitOp(
                    h1=h1,
                    mapper=mapper,
                    two_qubit_reduction=two_qubit_reduction,
                    z2symmetry_reduction=z2symmetry_reduction,
                    )
            for label, coef in qubit_h1.primitive.to_list():
                if qop is not None:
                    rop = PauliSumOp.from_list([
                            ('I'*qubit_h1.num_qubits, numpy.cos(theta*coef)),
                            (label, 1j*numpy.sin(theta*coef))
                            ])
                    if op is None:
                        op = rop
                    else:
                        op = (rop@op).reduce()
                if circ is None:
                    circ = QuantumCircuit(qubit_h1.num_qubits)
                circ = add_multiqubit_gate(label, theta*coef, circ)
    if qop is not None:
        rqop = (op@qop).reduce()
        rqop = (rqop@op.adjoint()).reduce()
    else:
        rqop = None

    return circ, rqop


@timeit
def doubly_decomposed_form(h1_D, h1_X, h2_D, h2_X, nn_qop):
    # getting Cholesky vectors for two body off-diag terms
    circs_and_rotH_arr = LRF_2_body_OD_terms(h2_X)
    # getting the givens rotation circuit and the rotated Hamiltonian for the one body terms
    qubitH_1_X = qubitOp(h1=h1_X)
    circ_1_body, rotH_1_X = get_givens_rotns(h1_X, qop=qubitH_1_X)
    circs_and_rotH_arr.append((circ_1_body, rotH_1_X))
    qubitH_D = qubitOp(h1=h1_D, h2=h2_D) + nn_qop
    circs_and_rotH_arr.append((QuantumCircuit(rotH_1_X.num_qubits), qubitH_D))
    return circs_and_rotH_arr


def expectation(h, circ):
    state = execute(circ, Aer.get_backend('statevector_simulator'),
                    shots=1).result().get_statevector()
    h_expec = state.conj()@h.to_matrix()@state
    return h_expec


def hamiltonian_expectation_in_doubly_decomposed_form_sv(params,
        ansatz,
        circs_and_rotH,
        qh_chk=None,
        ):
    ansatz_circ = ansatz.get_circuit(params)
    res = 0
    for circ, roth in circs_and_rotH:
        res += expectation(roth, ansatz_circ + circ).real
    print(f"energy (doubly decomposed): {res:.6f}")
    # consistent check
    if qh_chk is not None:
        h_sum_direct = expectation(qh_chk, ansatz_circ).real
        assert(abs(res-h_sum_direct) < 1.e-6)
    return res


@timeit
def hamiltonian_estimation_in_doubly_decomposed_form_qasm(
        ansatz,
        params,
        circs_and_rotH,
        shots=2**10,
        ):

    def Energy_Estimator_DD(circ, Op, shots):

        #@timeit
        def getCounts(circ, shots):
            circ.add_register(ClassicalRegister(circ.num_qubits, 'c'))
            clist = list(range(circ.num_qubits))
            circ.measure(clist, clist)
            job_sim = execute(circ,
                    AerSimulator(
                        method='density_matrix',
                        max_parallel_shots=8,
                        ),
                    shots=shots,
                    )
            res = job_sim.result()
            counts = res.get_counts(circ)
            return counts

        def estimate(PauliOp, counts):
            PauliString, coeff = PauliOp.primitive.to_list()[0]
            indexOfZs = numpy.where(numpy.array(list(PauliString)) == 'Z')[0]
            bitstrings, cnts = list(counts.keys()), list(counts.values())
            p = 0
            count_valid = 0
            for i in range(len(bitstrings)):
                bit_Str_to_Arr = numpy.array(list(bitstrings[i]))
                # flag1=1 if len(numpy.where(bit_Str_to_Arr=='1')[0])==4 else 0 #check number of electrons
                # flag2=1 if len(numpy.where(bit_Str_to_Arr[0:4]=='1')[0])==2 else 0 #check net spin
                # if ((flag1==1) and (flag2==1)):
                # count_valid=count_valid+cnts[i]
                counter_1 = list(numpy.array(list(bitstrings[i]))[
                                 indexOfZs]).count('1')
                if counter_1 % 2 == 1:
                    p = p+cnts[i]
            p = p/sum(cnts)
            mean = (1-2*p)
            return mean*coeff.real

        def meanOp(PauliOp, counts):
            mean = 0
            for i in range(len(PauliOp)):
                mean = mean+estimate(PauliOp[i], counts)
            return mean

        def covariance(Op, i, j, counts):
            cov = estimate(Op[i]@Op[j], counts) - \
                estimate(Op[i], counts)*estimate(Op[j], counts)
            return cov

        def covbtnOps(A, B, counts):
            corr = 0
            for i in range(len(A)):
                for j in range(len(B)):
                    corr = corr+estimate(A[i]@B[j], counts) - \
                        estimate(A[i], counts)*estimate(B[j], counts)
            return corr

        def variance(Op, counts):
            var = 0
            cov_mat = numpy.zeros((len(Op), len(Op)))
            for i in range(len(Op)):
                for j in range(i, len(Op)):
                    if i == j:
                        mean = estimate(Op[i], counts)
                        coeff = Op[i].primitive.to_list()[0][1].real
                        cov_mat[i][j] = (coeff**2-mean**2)
                        var = var+(coeff**2-mean**2)
                    elif i != j:
                        var = var+2*covariance(Op, i, j, counts)
                        cov_mat[i][j] = cov_mat[j][i] = covariance(
                            Op, i, j, counts)
            return var

        #@timeit
        def OpErr(data):
            Op, counts = data
            var = variance(Op, counts)
            err = numpy.sqrt(var/sum(counts.values()))
            return err.real

        counts_Arr = getCounts(circ, shots)
        m1 = meanOp(Op, counts_Arr)
        # err1 = OpErr((Op, counts_Arr))
        err1 = 0.001
        return m1, err1

    ansatz_circ = ansatz.get_circuit(params)

    results = Parallel(n_jobs=1, verbose=0)(
            delayed(Energy_Estimator_DD)
            (ansatz_circ+circh[0], circh[1], shots)
            for circh in circs_and_rotH)

    m1, err1 = results[0][0], results[0][1]
    m2, err2 = results[1][0], results[1][1]
    m3, err3 = results[2][0], results[2][1]
    print(f"total mean {m1+m2+m3:.6f} error {err1+err2+err3:.6f} shots: {shots}")
    if False:
        with open('OptStepsWithQasm_parity.txt', 'a') as f:
            Str = ["{:0.16f}".format(params[i]) for i in range(len(params))]
            print('['+','.join(Str)+']'+'#' +
                  "{:0.16f}".format(Energy(params, ansatz)), file=f)
    return m1+m2+m3


def Energy(
        params,
        ansatz,
        ):
    circ = ansatz.get_circuit(params)
    state = execute(circ, Aer.get_backend('statevector_simulator'),
                    shots=1).result().get_statevector()
    E = (state.conj()@Hmat@state).real
    return E


def Energy_sv(ansatz, params, shots=1):
    return Energy(params,
            ansatz,
            )

def commutatorPool(qubitH):
    labels = []
    pool = []
    for i, op1 in enumerate(qubitH):
        for op2 in qubitH[i+1:]:
            op12 = ((op1@op2).reduce() - (op2@op1).reduce()).reduce()
            assert(len(op12) == 1)
            op12 = op12.to_pauli_op()
            # remove coefficient
            op12._coeff = 1
            label = str(op12)
            if label in labels or label.count('Y')%2 == 0:
                continue
            labels.append(label)
            pool.append(op12)
    print(f'pool dim: {len(pool)}')
    print(f"pool: {labels}")
    return pool

def compute_gradient_sv(M,state):
    # -1j <HP - PH> for |\Psi> = e^{-iPt} |0>.
    grad = state@M@numpy.conjugate(state)
    grad *= -1j
    return grad.real


def compute_gradient_sv2(ansatz,
        op,
        params,
        qh_chk=None,
        ):
    ansatz.set_tmpcirc(str(op))
    E1 = hamiltonian_expectation_in_doubly_decomposed_form_sv(
            params + [numpy.pi/4],
            ansatz,
            circs_and_rotH,
            qh_chk=qh_chk,
            )
    E2 = hamiltonian_expectation_in_doubly_decomposed_form_sv(
            params + [-numpy.pi/4],
            ansatz,
            circs_and_rotH,
            qh_chk=qh_chk,
            )
    gradient = E1-E2
    return gradient


def compute_gradient(ansatz,
        op,
        params,
        shots=2**10,
        ):
    ansatz.set_tmpcirc(str(op))
    E1 = hamiltonian_estimation_in_doubly_decomposed_form_qasm(
            ansatz,
            params + [numpy.pi/4.],
            circs_and_rotH,
            shots=shots,
            )
    E2 = hamiltonian_estimation_in_doubly_decomposed_form_qasm(
            ansatz,
            params+[-numpy.pi/4.],
            circs_and_rotH,
            shots=shots,
            )
    gradient = E1-E2
    return gradient


# In[176]:


def SMO_qasm(cost,
        ansatz,
        params,
        circs_and_rotH=None,
        runs=40,
        tol=1e-4,
        shots=2**10,
        save_opt_steps=False,
        ):

    # def Energy(ansatz, params):
    #     backend = Aer.get_backend('statevector_simulator')
    #     circ = ansatz.get_circuit(params)
    #     stateVector_0 = execute(
    #         circ, backend, shots=1).result().get_statevector()
    #     E = numpy.real(
    #         numpy.dot(numpy.dot(numpy.conjugate(stateVector_0), Hmat), stateVector_0))
    #     return E

    def E_landscape(ind, ang, cost, ansatz, params):
        params1 = copy.deepcopy(params)
        params1[ind] = params1[ind]+ang  # ang
        data = cost(ansatz,
                params1,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                )
        return data

    def determine_unknowns(E, cost, ansatz, params, ind):
        L1 = E
        L2 = E_landscape(ind, numpy.pi/4., cost, ansatz, params)
        L3 = E_landscape(ind, -numpy.pi/4., cost, ansatz, params)
        ratio = (L3-L2)/(2*L1-L2-L3)
        a3 = (L2+L3)/2.
        a2 = 2*params[ind]-numpy.arctan(ratio)
        a1 = (L1-a3)/numpy.cos(numpy.arctan(ratio))
        return a1, a2, a3

    def update(E, cost, ansatz, params, ind):
        t1 = time.time()
        a1, a2, a3 = determine_unknowns(E, cost, ansatz, params, ind)
        thetaStar = a2/2.+numpy.pi/2. if a1 > 0 else a2/2.
        newParams = copy.deepcopy(params)
        newParams[ind] = thetaStar
        # for low shots, this can be off.
        # updEnergy = a1*numpy.cos(2*thetaStar-a2)+a3

        updEnergy = cost(ansatz,
                newParams,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                )

        return newParams, updEnergy


    # current energy
    Eold = E = cost(ansatz,
            params,
            circs_and_rotH=circs_and_rotH,
            shots=shots,
            )
    # reverse order
    inds = numpy.arange(len(params))[::-1]

    for iloop in range(runs):
        print("looped "+str(iloop)+" times")
        for ind in inds:
            params, E = update(E, cost, ansatz, params, ind)
            # print("upd_energy", E)
            # if save_opt_steps == True:
            #     with open('OptStepsWithQasm_parity.txt', 'a') as f:
            #         Str = ["{:0.16f}".format(params[i])
            #                for i in range(len(params))]
            #         print('['+','.join(Str)+']'+'#' +
            #               "{:0.16f}".format(E), file=f)
            # else:
            #     continue
        conv_err = Eold - E
        print("loop energy", E)
        print("inner loop error", conv_err)
        Eold = E

    # Eval = cost(ansatz,
    #         params,
    #         circs_and_rotH=circs_and_rotH,
    #         shots=shots,
    #         )
    # print(f'E-interp: {E:.6f}, Eval: {Eval:.6f}')

    res = {'x': params, 'fun': E}
    return res


def fsmo(x, a, b, c):
    return a*numpy.cos(2*x - b) + c


def SMO2_qasm(cost,
        ansatz,
        params,
        circs_and_rotH=None,
        runs=40,
        shots=2**10,
        ):

    def E_landscape(ind, ang, cost, ansatz, params):
        params1 = copy.deepcopy(params)
        params1[ind] = ang
        data = cost(ansatz,
                params1,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                )
        return data

    def determine_unknowns(E, cost, ansatz, params, ind):
        thetas = []
        vals = []
        for i in range(-4, 4):
            theta = i*numpy.pi/8
            thetas.append(theta)
            val = E_landscape(ind, theta, cost, ansatz, params)
            vals.append(val)
        # initial guess
        L1, L2, L3 = vals[4], vals[6], vals[2]
        ratio = (L3-L2)/(2*L1-L2-L3)
        a3 = (L2+L3)/2.
        a2 = -numpy.arctan(ratio)
        a1 = (L1-a3)/numpy.cos(numpy.arctan(ratio))
        # fitting
        from scipy.optimize import curve_fit
        popt, pcov = curve_fit(fsmo, thetas, vals)
        err = sum([abs(val - fsmo(th, *popt)) for val, th in zip(vals, thetas)])
        err /= len(thetas)
        print(f'fitting err: {err:.6f}')
        return popt

    def update(E, cost, ansatz, params, ind):
        t1 = time.time()
        a1, a2, a3 = determine_unknowns(E, cost, ansatz, params, ind)
        thetaStar = a2/2.+numpy.pi/2. if a1 > 0 else a2/2.
        newParams = copy.deepcopy(params)
        newParams[ind] = thetaStar
        # for low shots, this can be off.
        updEnergy_fit = fsmo(thetaStar, a1, a2, a3)
        updEnergy = cost(ansatz,
                newParams,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                )
        print(f'upd energy: fit={updEnergy_fit:.6f} calc={updEnergy:.6f}')
        return newParams, updEnergy


    # current energy
    Eold = E = cost(ansatz,
            params,
            circs_and_rotH=circs_and_rotH,
            shots=shots,
            )
    # reverse order
    inds = numpy.arange(len(params))[::-1]

    for iloop in range(runs):
        print("looped "+str(iloop)+" times")
        for ind in inds:
            params, E = update(E, cost, ansatz, params, ind)
        conv_err = Eold - E
        print("loop energy", E)
        print("inner loop error", conv_err)
        Eold = E
    res = {'x': params, 'fun': E}
    return res
# Adadelta with Qasm


def Adagradient_qasm(cost,
        ansatz,
        params,
        circs_and_rotH=None,
        runs=20,
        tol=1e-4,
        shots=2**10,
        ):
    scale = 1
    params = numpy.asarray(params)
    directions = numpy.arange(len(params))
    Eold = cost(ansatz,
            params,
            circs_and_rotH=circs_and_rotH,
            shots=shots,
            )

    def StochGrad(cost,
            params,
            directions,
            circs_and_rotH,
            shots,
            ):
        shiftedParams1 = [numpy.array(params)
                +numpy.array([0]*ei+[numpy.pi/4.]+[0]*(len(params)-ei-1))
                for ei in directions]
        shiftedParams2 = [numpy.array(params)
                +numpy.array([0]*ei+[-numpy.pi/4.]+[0]*(len(params)-ei-1))
                for ei in directions]

        results1 = Parallel(n_jobs=1, verbose=0)(
            delayed(hamiltonian_estimation_in_doubly_decomposed_form_qasm)
            (ansatz, params, circs_and_rotH, shots)
            for params in shiftedParams1)

        results2 = Parallel(n_jobs=1, verbose=0)(
            delayed(hamiltonian_estimation_in_doubly_decomposed_form_qasm)
            (ansatz, params, circs_and_rotH, shots)
            for params in shiftedParams2)

        stochGrads = numpy.array(results1)-numpy.array(results2)
        return stochGrads

    for iloop in range(runs):
        g_stoch = StochGrad(cost, params, directions, circs_and_rotH, shots)
        params1 = params - scale*g_stoch
        E = cost(ansatz,
            params1,
            circs_and_rotH=circs_and_rotH,
            shots=shots,
            )
        while E >= Eold and scale >= 0.005:
            scale /= 2
            params1 = params - scale*g_stoch
            E = cost(ansatz,
                params1,
                circs_and_rotH=circs_and_rotH,
                shots=shots,
                )
        if scale < 0.005:
            break
        print(f'i-adagradient: {iloop} e: {E:.6f}')
        Eold = E
        params = params1

    print(f'final e: {E:.6f}')
    res = {'x': params.tolist(), 'fun': E}
    return res


def AdaDelta_qasm(cost,
        params,
        runs=20,
        tol=1e-4,
        num_shots_arr=[2**8, 2**9, 2**9],
        save_opt_steps=False,
        ):
    string_shots_label = ','.join([str(shots) for shots in num_shots_arr])
    v = numpy.zeros((len(params)))
    delta = numpy.zeros((len(params)))
    beta = 0.9
    rho = 0.8
    directions = numpy.arange(len(params))
    delta = 0
    conv_err = 1
    old_conv_err = 1
    Eold = cost(params, num_shots_arr)
    E_arr = []
    ind = 0
    eps = numpy.array([5e-8]*len(params))  # 1e-5,5e-6,1e-7,5e-8
    Max = 1e5
    lr0 = 0.05

    def learningRateEnv(lr0, runs):
        lr_u = lr0+Max*numpy.exp(-(28/runs)*numpy.linspace(0, runs, runs+1))
        lr_d = lr0*(1-numpy.exp(-(28/runs)*numpy.linspace(0, runs, runs+1)))
        return lr_u, lr_d

    # def Energy(params):
    #     backend = Aer.get_backend('statevector_simulator')
    #     circ = var_form_base.construct_circuit(parameters=params)
    #     stateVector_0 = execute(
    #         circ, backend, shots=1).result().get_statevector()
    #     E = numpy.real(
    #         numpy.dot(numpy.dot(numpy.conjugate(stateVector_0), Hmat), stateVector_0))
    #     return E

    def StochGrad(cost, params, directions, num_shots_arr):
        def Energy_Estimator_DD(input_vars):

            def getCounts(Input):
                circ, num_shots = Input
                circ1 = circ.copy()
                circ1.add_register(ClassicalRegister(6, 'c'))
                circ1.measure([0, 1, 2, 3, 4, 5], [0, 1, 2, 3, 4, 5])
                job_sim = execute(circ1, AerSimulator(
                    method='density_matrix'), shots=num_shots)
                result_sim = job_sim.result()
                #density_matrix = result_sim
                counts = result_sim.get_counts(circ1)
                return counts  # ,density_matrix

            def estimate(PauliOp, counts):
                PauliString, coeff = PauliOp.primitive.to_list()[0]
                indexOfZs = numpy.where(
                    numpy.array(list(PauliString)) == 'Z')[0]
                bitstrings, cnts = list(counts. keys()), list(counts.values())
                p = 0
                count_valid = 0
                for i in range(len(bitstrings)):
                    bit_Str_to_Arr = numpy.array(list(bitstrings[i]))
                    counter_1 = list(numpy.array(list(bitstrings[i]))[
                                     indexOfZs]).count('1')
                    if counter_1 % 2 == 1:
                        p = p+cnts[i]
                p = p/sum(cnts)
                mean = (1-2*p)
                return mean*coeff.real

            def meanOp(PauliOp, counts):
                mean = 0
                for i in range(len(PauliOp)):
                    mean = mean+estimate(PauliOp[i], counts)
                return mean

            def covariance(Op, i, j, counts):
                cov = estimate(Op[i]@Op[j], counts) - \
                    estimate(Op[i], counts)*estimate(Op[j], counts)
                return cov

            def covbtnOps(A, B, counts):
                corr = 0
                for i in range(len(A)):
                    for j in range(len(B)):
                        corr = corr + \
                            estimate(A[i]@B[j], counts)-estimate(A[i],
                                    counts)*estimate(B[j], counts)
                return corr

            def variance(Op, counts):
                var = 0
                cov_mat = numpy.zeros((len(Op), len(Op)))
                for i in range(len(Op)):
                    for j in range(i, len(Op)):
                        if i == j:
                            mean = estimate(Op[i], counts)
                            coeff = Op[i].primitive.to_list()[0][1].real
                            cov_mat[i][j] = (coeff**2-mean**2)
                            var = var+(coeff**2-mean**2)
                        elif i != j:
                            var = var+2*covariance(Op, i, j, counts)
                            cov_mat[i][j] = cov_mat[j][i] = covariance(
                                Op, i, j, counts)
                return var

            def OpErr(data):
                Op, counts = data
                var = variance(Op, counts)
                err = numpy.sqrt(var/sum(counts.values()))
                return err.real
            circ, Op, num_shots = input_vars
            counts_Arr = getCounts((circ, num_shots))
            m1 = meanOp(Op, counts_Arr)
            err1 = OpErr((Op, counts_Arr))
            return m1, err1
        shiftedParams1 = [numpy.array(
            params)+numpy.array([0]*ei+[numpy.pi/4.]+[0]*(len(params)-ei-1)) for ei in directions]
        shiftedParams2 = [numpy.array(
            params)+numpy.array([0]*ei+[-numpy.pi/4.]+[0]*(len(params)-ei-1)) for ei in directions]
        stochGrads = numpy.zeros((len(params)))
        data_arr = []
        for i in range(len(shiftedParams1)):
            circ = var_form_base.construct_circuit(
                parameters=shiftedParams1[i])
            circ_arr = [circ.copy(), circ.copy(), circ.copy()]
            circ_arr[0] = circ_arr[0]+circs_and_rotH[0][0]
            circ_arr[1] = circ_arr[1]+circs_and_rotH[1][0]
            data_arr = data_arr+[(circ_arr[0], circs_and_rotH[0][1], num_shots_arr[0]),
                                 (circ_arr[1], circs_and_rotH[1]
                                  [1], num_shots_arr[1]),
                                 (circ_arr[2], circs_and_rotH[2], num_shots_arr[2])]
        for i in range(len(shiftedParams2)):
            circ = var_form_base.construct_circuit(
                parameters=shiftedParams2[i])
            circ_arr = [circ.copy(), circ.copy(), circ.copy()]
            circ_arr[0] = circ_arr[0]+circs_and_rotH[0][0]
            circ_arr[1] = circ_arr[1]+circs_and_rotH[1][0]
            data_arr = data_arr+[(circ_arr[0], circs_and_rotH[0][1], num_shots_arr[0]),
                                 (circ_arr[1], circs_and_rotH[1]
                                  [1], num_shots_arr[1]),
                                 (circ_arr[2], circs_and_rotH[2], num_shots_arr[2])]
        results = Parallel(n_jobs=1, verbose=0)(
            delayed(Energy_Estimator_DD)(data_arr[i]) for i in range(len(data_arr)))
        estm1Arr = [results[i][0]+results[i+1][0]+results[i+2][0]
                    for i in range(0, len(results)//2, 3)]  # map(cost,shiftedParams1)
        estm2Arr = [results[i][0]+results[i+1][0]+results[i+2][0]
                    for i in range(len(results)//2, len(results), 3)]
        stochGrads = 0.5*(numpy.array(list(estm1Arr)) -
                          numpy.array(list(estm2Arr)))
        return stochGrads
    lr_u, lr_d = learningRateEnv(lr0, runs)
    flag = 0
    flag1 = 0
    terminate = False
    while ((conv_err > tol) and (ind < runs)):
        print("index", ind)
        old_conv_err = conv_err
        g_stoch = StochGrad(cost, params, directions, num_shots_arr)
        v = beta*v+(1-beta)*g_stoch*g_stoch
        lr = numpy.sqrt(delta+eps)/numpy.sqrt(v+eps)  # learning rate
        # clipping learning rates, increasing shots and restarting ada delta
        if ((conv_err < 1e-4) and (flag == 0)):
            flag = 1
            num_shots_arr = [2**19, 2**18, 2**19]
            print("************shots increased**************")
        if ((conv_err < 5e-7) and (flag1 == 0)):
            flag1 = 1
            num_shots_arr = [2**21, 2**20, 2**21]
            print("************shots increased**************")
        for i in range(len(lr)):
            if lr[i] > lr_u[ind]:
                eps[i] = lr_u[ind]*lr_u[ind]*v[i]
                lr[i] = lr_u[ind]
                delta[i] = 0
            elif lr[i] < lr_d[ind]:
                eps[i] = lr_d[ind]*lr_d[ind]*v[i]
                lr[i] = lr_d[ind]
                delta[i] = 0
            else:
                continue
        g = lr*g_stoch
        params = params-g
        params = params.real
        delta = beta*delta+(1-beta)*g*g
        E = Energy(params)  # HamiltonianEstm(params)[0]
        E_arr.append(E)
        print("updated energy", E, "learning rate", lr)  # ,Energy(params))
        if save_opt_steps == True:
            with open('OptStepsWithQasm_parity.txt', 'a') as f:
                print('['+','.join([str(params[i])
                      for i in range(len(params))])+']'+'#'+str(E), file=f)
        ind = ind+1
        conv_err = rho*conv_err+(1-rho)*numpy.abs(Eold-E)
        print("error", conv_err)
        Eold = E
    res = {'x': params, 'fun': E}
    return res


# In[177]:

h1_D, h1_X, h2_D, h2_X = egBandHamiltonianPartition()

# adding chemical potential
# chem_pot = 14.5
# h1_D -= numpy.eye(h1_D.shape[0])*chem_pot

qubitH = qubitOp(h1=h1_D+h1_X,
        h2=h2_D+h2_X,
        )

# commutator pool
commutator_pool = commutatorPool(qubitH)

# breakpoint()

# penalty term, disadvantageous as it increase the energy range of H
# weight = 10.
# ne = len(h1_D)//2
# nn_fop = get_fermion_nnop(h1=numpy.eye(h1_D.shape[0]))
# nn_fop = (nn_fop - FermionicOp(('', ne))).reduce()
# nn_fop = (nn_fop@nn_fop).reduce()
# nn_qop = qubitOp(fop=nn_fop)
# nn_qop*= weight
# qubitH += nn_qop

# alternatively, chemical potential
chem_pot = -9.778
n_fop = get_fermion_nnop(h1=numpy.eye(h1_D.shape[0]))
n_qop = qubitOp(fop=n_fop)
n_qop *= chem_pot
qubitH += n_qop
qubitH.reduce()

# doubly factorized form
#circs_and_rotH = doubly_decomposed_form(h1_D, h1_X, h2_D, h2_X, nn_qop)
circs_and_rotH = doubly_decomposed_form(h1_D, h1_X, h2_D, h2_X, n_qop)


Hmat = qubitH.to_matrix().real
w, v = numpy.linalg.eigh(Hmat)
Eg = w[0]
print(f'Eg = {Eg:.6f}')

n_op = qubitOp(h1=numpy.eye(h1_D.shape[0]))
print(f'n fermion: {v[:,0].dot(n_op.to_matrix()).dot(v[:,0].conj()):.4f}')

# constructing matrices for gradient computation
MatrixRepOfPoolOps = [(qubitH@op - op@qubitH).reduce().to_matrix()
        for op in commutator_pool]

backend = Aer.get_backend('statevector_simulator')

nq = 6
ansatz = paramCircuits(nq,
        ref_state='001001',
        )

error = 1000

# os.environ["QISKIT_NUM_PROCS"] = '8'
# os.environ["QISKIT_IN_PARALLEL"] = "TRUE"

params = []
EnergyArr = []
ExcOps = []
ti = time.time()
steps = 35

# we can change this to [2**18,2**18,2**18] it still works
for i in range(35):
    # save_steps=False

    # state-vector
    # circ = ansatz.get_circuit(params)
    # state=execute(circ,backend,shots=1).result().get_statevector()
    # grads=numpy.array(Parallel(n_jobs=1,verbose=0) \
    #         (delayed(compute_gradient_sv)(MatrixRepOfPoolOps[i],state) \
    #         for i in range(len(commutator_pool))))

    # grads=numpy.array(Parallel(n_jobs=1,verbose=0) \
    #         (delayed(compute_gradient_sv2)(ansatz, op, params, qubitH) \
    #         for op in commutator_pool))
    # assert(numpy.allclose(grads1, grads))

    # qasm
    grads = numpy.array(Parallel(n_jobs=1, verbose=0)(
            delayed(compute_gradient)(ansatz, op, params, shots=2**10)
            for op in commutator_pool))

    ansatz.del_tmpcirc()
    indexes = numpy.argsort(abs(grads))[::-1]
    print("highest grads:",
            [numpy.round(grads[i], decimals=4) for i in indexes])

    if len(ExcOps) > 0:
        if (str(commutator_pool[indexes[0]]) != ExcOps[-1]):
            PauliOp = commutator_pool[indexes[0]]
        else:
            PauliOp = commutator_pool[indexes[1]]
    else:
        PauliOp = commutator_pool[indexes[0]]

    ExcOps.append(str(PauliOp))
    print(f"i: {i} chosen Op", ExcOps[-1])
    params.append(0.0)
    ansatz.expand(str(PauliOp))

    print(f'generators: {ExcOps}')

    # state-vector
    # res = scipy.optimize.minimize(Energy, params, args=(ansatz), method='BFGS',
    #         tol=1e-4)
    # res = SMO_qasm(
    #         Energy_sv,
    #         ansatz,
    #         params,
    #         runs=50,
    #         tol=1e-4,
    #         save_opt_steps=False,
    #         )

    # res = AdaDelta_qasm(
    #         Energy_sv,
    #         params,
    #         runs=200, tol=1e-6, num_shots_arr=[2**18, 2**18, 2**18],
    #         )

    # qasm
    # res = SMO_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,
    #          ansatz,
    #          params,
    #          circs_and_rotH=circs_and_rotH,
    #          runs=40,
    #          tol=1e-4,
    #          save_opt_steps=False,
    #          )

    runs = min(40, 1+i*10)
    res = SMO2_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,
             ansatz,
             params,
             circs_and_rotH=circs_and_rotH,
             runs=runs,
             )


    # res = Adagradient_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,
    #         ansatz,
    #         params,
    #         circs_and_rotH=circs_and_rotH,
    #         runs=200,
    #         tol=1e-4,
    #         )

    # params, E = list(res['x']), res['fun']

    # SMO_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm,params,runs=40,tol=1e-6,save_opt_steps=True)
    # res = AdaDelta_qasm(hamiltonian_estimation_in_doubly_decomposed_form_qasm, params,
    #        runs=200, tol=1e-6, num_shots_arr=[2**18, 2**18, 2**18], save_opt_steps=True)

    params, E = list(res['x']), res['fun']
    print("num_params", len(params))
    print("Energy", E)
    EnergyArr.append(E)

    e_sv = Energy_sv(ansatz, params)
    print(f'iter {i} Energy_sv = {e_sv:.6f}')
    print("time elapsed", time.time()-ti)


# In[167]:


indexes = numpy.argsort(abs(grads))[::-1][:5]
label = [commutator_pool[i].paulis[0][1].to_label()
         for i in range(len(indexes))]
