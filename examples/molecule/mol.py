# author: Yongxin Yao (yxphysics@gmail.com), Jacob Brunton
from qiskit_nature.drivers import Molecule
from qiskit_nature.drivers.second_quantization import (
        ElectronicStructureDriverType,
        ElectronicStructureMoleculeDriver,
        )
from qiskit_nature.problems.second_quantization.electronic import (
        ElectronicStructureProblem,
        )
from qiskit_nature.properties.second_quantization.electronic import (
        ParticleNumber,
        )
from qiskit_nature.transformers.second_quantization.electronic import (
        ActiveSpaceTransformer,
        )
from qiskit_nature.algorithms.ground_state_solvers import \
        GroundStateEigensolver
from qiskit_nature.circuit.library import (UCCSD, HartreeFock,
        )
from qiskit.algorithms import NumPyMinimumEigensolver
from qiskit.opflow import TwoQubitReduction
import numpy as np
import h5py, json



class molpol:
    def __init__(self):
        self.get_inp()
        self.set_ham()
        self.check_gs()

    def get_inp(self):
        vglobal = {}
        vlocal = {}
        exec(open("inp.py", "r").read(), vglobal, vlocal)
        self._inp = vlocal["inp"]

    def set_ham(self):
        inp = self._inp
        molecule = Molecule(
                geometry=inp["geometry"],
                charge=inp["charge"],
                multiplicity=inp["multiplicity"],
                )
        driver = ElectronicStructureMoleculeDriver(
                molecule,
                basis=inp["basis"],
                driver_type=ElectronicStructureDriverType.PYSCF,
                )
        properties = driver.run()
        print(f"pyscf res: {properties}")

        particle_number = properties.get_property(ParticleNumber)
        print(f"particle_number = {particle_number}")

        active_space_trafo = ActiveSpaceTransformer(
                num_electrons=sum(particle_number.num_particles)\
                        -inp["delta_nelec"],
                num_molecular_orbitals=len(inp["active_orbitals"]),
                active_orbitals=inp["active_orbitals"],
                )

        problem = ElectronicStructureProblem(driver,
                transformers=[active_space_trafo])
        second_q_ops = problem.second_q_ops()
        num_spin_orbitals = problem.num_spin_orbitals
        num_particles = problem.num_particles

        qubit_converter = inp["qubit_converter"]
        groundstate_solver = GroundStateEigensolver(qubit_converter,
                NumPyMinimumEigensolver())
        result = groundstate_solver.solve(problem)
        print(result)
        self._enuc = result.nuclear_repulsion_energy
        target_energy = np.real(result.eigenenergies + self._enuc)[0]
        print(f"Reference gs total energy: {target_energy:.6f}")
        print(f"Reference gs electron energy: {result.eigenenergies[0].real:.6f}")
        self._target_energy = target_energy
        # here it also sets converter._num_particles !!!
        # so one does not need reducer.
        self._qham = qubit_converter.convert(second_q_ops["ElectronicEnergy"],
                num_particles=num_particles)
        # dipole_z operator
        self._dipop = qubit_converter.convert(second_q_ops["DipoleMomentZ"],
                num_particles=num_particles)
        # Hartree-Fock reference state
        init_state = HartreeFock(
                num_spin_orbitals,
                num_particles,
                qubit_converter,
                )

        init_state = HartreeFock(num_spin_orbitals, num_particles, qubit_converter)
        var_form = UCCSD(qubit_converter,
                num_particles,
                num_spin_orbitals,
                initial_state=init_state)

        one = 'I'*var_form.num_qubits
        pool = [str(p) for op in var_form.operators
                for p in op.primitive.paulis[:] if str(p) != one]

        print(f'Pool dimension: {len(pool)}')

        # potentially removing Z's
        pool = list(set([op.replace("Z", "I") for op in pool]))
        print(f'Pool-no-Z dimension: {len(pool)}')

        # incar data
        hs_list = [f"{p.coeffs[0].real:16.12f}*{p.paulis[0]}"
                for p in self._qham.primitive]
        print(f"Hamiltonian pauli terms: {len(hs_list)}")

        # dip_op
        dip_list = [f"{p.coeffs[0].real:16.12f}*{p.paulis[0]}"
                for p in self._dipop.primitive]

        # generate HF label
        label = ''.join(x for x in str(init_state) if x.isdigit() or x == "X")
        hf_label = ""
        for i in range(var_form.num_qubits):
            label = label[label.index(str(i))+len(str(i)):]
            if len(label) > 0 and label[0] == "X":
                hf_label += "1"
            else:
                hf_label += "0"

        data = {"h": hs_list,
                "dipole_z": dip_list,
                "pool": pool,
                "nume": sum(problem.num_particles),
                "ref_state_circ": str(init_state),
                "ref_state": hf_label[::-1],
                }
        with open("incar", "w") as f:
            json.dump(data, f, indent=4)


    def check_gs(self):
        h = self._qham.to_matrix()
        evals, evecs = np.linalg.eigh(h)
        gs_e = evals[0]
        gs_v = evecs[:, 0]
        self._gs_v = gs_v
        energy = gs_e + self._enuc
        print(f'qubit Hamiltonian gs energy: {energy:.6f}')
        assert(abs(energy - self._target_energy) < 1e-6)
        dipop = self._dipop.to_matrix()
        dip_z = gs_v.dot(dipop).dot(gs_v.T.conj())
        print(f'dip_z: {dip_z:.6f}')



if __name__ == "__main__":
    mol = molpol()
