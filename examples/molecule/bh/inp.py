from qiskit_nature.converters.second_quantization import QubitConverter
from qiskit_nature.mappers.second_quantization import ParityMapper



inp = {
        "geometry": [["B",[0.0, 0.0, 0.0]], ["H", [0.0, 0.0, 1.2324]]],
        "charge": 0,
        "multiplicity": 1,
        "basis": "sto3g",
        "delta_nelec": 2,
        "active_orbitals": range(1,6),
        "qubit_converter": QubitConverter(ParityMapper(),
                two_qubit_reduction=True,
                ),
        }
