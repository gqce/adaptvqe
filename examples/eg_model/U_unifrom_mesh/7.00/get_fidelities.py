import subprocess, numpy


cmd = ["/home/ykent/GitLab/cyqc/adaptvqe/adaptvqe/run.py", "--includepoolz",
        "-p", "1", "-m", "1", "-a", "1", "-t", "0.0000001", "-n", "0"]
fidelities = []
# for i in range(32):
for i in [24]:
    cmd[-1] = str(i)
    print(' '.join(cmd))
    process = subprocess.run(cmd, check=True, capture_output=True)
    res = process.stdout[:-1].decode()
    ind = res.index('final:')
    print(res[ind+6:])
    fid = float(res[ind+6: ind+20])
    print(i, fid)
    fidelities.append(fid)

numpy.savetxt('fidelities.dat', fidelities)
