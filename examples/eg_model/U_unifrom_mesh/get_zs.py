import numpy, os, h5py



# set hund's j / hubbard u
joveru = 0.3
norb = 2
# embedding Hamiltonian solver
iembeddiag = -2  # ED with Sz symmetry
# electron filling
num_e = norb + 1

ulist = numpy.arange(0.5, 8.1, 0.5)
z_list = []

for u in ulist:
    print(f' working on u = {u}')
    path = f"{u:.2f}"
    os.chdir(path)
    with h5py.File('GLog.h5', 'r') as f:
        z = f['/impurity_0/R'][0,0]**2
    z_list.append(z)
    os.chdir('../')

numpy.savetxt('z_u.dat', z_list)
