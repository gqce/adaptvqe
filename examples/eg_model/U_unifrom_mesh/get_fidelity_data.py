import numpy, os



# set hund's j / hubbard u
joveru = 0.3
norb = 2
# embedding Hamiltonian solver
iembeddiag = -2  # ED with Sz symmetry
# electron filling
num_e = norb + 1

ulist = numpy.arange(0.5, 8.1, 0.5)
fidelities_list = []

for u in ulist:
    print(f' working on u = {u}')
    path = f"{u:.2f}"
    os.chdir(path)
    fidelities_list.append(numpy.loadtxt('fidelities.dat').reshape(-1))
    os.chdir('../')

numpy.savetxt('fidelities_u.dat', fidelities_list)
