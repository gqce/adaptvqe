import subprocess, numpy


cmd = ["/home/ykent/GitLab/cyqc/adaptvqe/adaptvqe/run.py", "--includepoolz",
        "-p", "1", "-m", "1", "-a", "1", "-t", "0.0000001", "-n", "0"]
fidelities = []
for i in range(32):
    cmd[-1] = str(i)
    process = subprocess.run(cmd, check=True, capture_output=True)
    res = process.stdout[:-1].decode()

    key = 'lowest energies:'
    ind = res.index(key) + len(key) + 3
    e_exact = float(res[ind:ind+11])
    # print('e_exact', e_exact)

    key = 'cost:'
    ind = res.index(key) + len(key)
    e_est = float(res[ind:ind+11])
    e_err = e_est - e_exact
    # print('err:', e_err)

    ind = res.index('final:')
    fid = float(res[ind+6: ind+20])
    fidelities.append([fid, e_err])

numpy.savetxt('fidelities.dat', fidelities)
