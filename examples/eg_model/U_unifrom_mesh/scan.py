import numpy, os, h5py, shutil
from pygrisb.model.mbandhubbardsemicir import gutz_model_setup
from pygrisb.run.cygutz import run_cygutz


# set hund's j / hubbard u
joveru = 0.3
norb = 2
# embedding Hamiltonian solver
iembeddiag = -2  # ED with Sz symmetry
# electron filling
num_e = norb + 1

ulist = numpy.arange(0, 8.1, 0.5)
zlist = []

for u in ulist:
    print(f' working on u = {u}')
    path = f"{u:.2f}"
    if not os.path.isdir(path):
        os.mkdir(path)
    os.chdir(path)
    if os.path.isfile('../GLog.h5'):
            shutil.copy('../GLog.h5', '.')
    # generate input files for gutzwiller calculations
    gutz_model_setup(
            norb=norb,
            u=u,
            j=u*joveru,
            iembeddiag=iembeddiag,
            num_e=num_e,
            )

    # perform the *CyGutz* calculation.
    run_cygutz(
            rmethod="hybr",
            cmdlargs=False,
            )

    # get quasiparticle weight Z = R^\dagger R
    with h5py.File('GLog.h5', 'r') as f:
        # diagonal in this case
        r = f['/impurity_0/R'][0, 0]
        z = r*r.conj()
        zlist.append(z.real)

    shutil.copy("GLog.h5", '../')
    os.chdir("..")


# save data
with open("uz.dat", "w") as f:
    for u, z in zip(ulist, zlist):
        f.write(f"{u:.2f} {z:.4f}\n")


